import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './views/pages/authentication/auth.guard';
import { AuthComponent } from './views/themes/auth/auth.component';
import { BaseComponent } from './views/themes/base/base.component';


const routes: Routes = [
  { path: '' , redirectTo: 'public', pathMatch: 'full' },
  { path: 'sys' , redirectTo: 'system', pathMatch: 'full' },
  {
    path: 'public',
    component: BaseComponent,
    children: [
      {path: 'p/calendar', loadChildren: () => import('./views/pages/calendar/calendar.module').then(m => m.CalendarModule)},
      {path: 'p/auth', loadChildren: () => import('./views/pages/authentication/authentication.module').then(m => m.AuthenticationModule)},
      {path: '' , redirectTo: 'p/calendar', pathMatch: 'full'},
    ]
  },
  {
    path: 'system',
    component: AuthComponent,
    canActivate: [ AuthGuard ],
    children: [
      {path : 's/calendar', loadChildren: () => import('./views/pages/calendar/calendar.module').then(m => m.CalendarModule)},
      {path: 's/booking', loadChildren: () => import('./views/pages/booking/booking.module').then(m => m.BookingModule)},
      {path: 's/itsetup', loadChildren : () => import('./views/pages/setup/setup.module').then(m => m.SetupModule)},
      {path: 's/user', loadChildren: () => import('./views/pages/user-management/user-management.module').then(m => m.UserManagementModule)},
      {path: 's/system', loadChildren: () => import('./views/pages/system-setting/system-setting.module').then(m => m.SystemSettingModule)},
      {path: 's/report', loadChildren: () => import('./views/pages/report/report.module').then(m => m.ReportModule)},
      {path: '' , redirectTo: 's/calendar', pathMatch: 'full'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
