import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { GetService } from '../../../../../core/get.service'
import { PostService } from '../../../../../core/post.service'
import { PatchService } from '../../../../../core/patch.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-table-register',
  templateUrl: './table-register.component.html',
  styleUrls: ['./table-register.component.css']
})
export class TableRegisterComponent implements OnInit {

  myForm: FormGroup
  notFound = ""
  loading = false
  pageTitle
  arrData:any = {}

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _post: PostService,
    private _get: GetService,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this.formInit()
    this.activatedRoute.params.subscribe(params => {
      const id = params.id
      if(id){
        this.pageTitle= "Edit"
        this._get.getSingleDepartment(id)
        .subscribe(
          res => {
            this.arrData = res
            this.arrData.id = id
            if(this.arrData.notFound === 1){
              //display no data found
              this.loading = true
              this.notFound = "Table ID is not found"
            }else{
              this.myForm = this.fb.group({
                tableSetup : [this.arrData.tableSetup, [Validators.required]]
              })
            }
          }
        )
      }else{
        this.pageTitle= "Register new"
        this.arrData.id = ''
      }
    })
  }

  formInit(){
    this.myForm = this.fb.group({
      tableSetup : ['', [Validators.required]]
    })
  }

  onSubmit(){
    this.loading = true
    if(this.arrData.id != ''){
      this.onPatch()
    }else{
      this.onRegister()
    }
  }

  onRegister(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    this._post.postTable(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: res.tableSetup + " successfully registered",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          this.formInit()
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }

  onPatch(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    formData.id = this.arrData.id
    this._patch.editTable(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: formData.tableSetup + " successfully updated",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          setTimeout(() =>this.router.navigate(['/system/s/system/table-list']) ,500)
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }

  get tableSetup(){ return this.myForm.get('tableSetup') }

}
