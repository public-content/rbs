import { Component, OnInit } from '@angular/core';
import { GetService } from '../../../../../core/get.service';
import { PatchService } from '../../../../../core/patch.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

  tableList;

  constructor(
    private _get: GetService,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this._get.getAllTable().subscribe(data => this.tableList = data)
  }

  getCount(){
    if(this.tableList){
      const active = this.tableList.filter(e => {
        return e.status === "active"
      })

      const deleted = this.tableList.filter(e => {
        return e.status === "deleted"
      })

      return "Active : "+active.length+" | Deleted : "+deleted.length
    }
  }

  onDelete(id, type, unit){
    const formData: any = {}
    formData.id = id
    formData.name = unit
    formData.person = localStorage.getItem('rsesu')
    if(type == 'delete'){
      formData.status = 'deleted'
      formData.msg = 'has deleted'
    }else if(type == 'activate'){
      formData.status = "active"
      formData.msg = "has activated"
    }

    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes"
    }).then((result) => {
      if (result.value) {
        this._patch.delTable(formData)
        .subscribe(
          res => {
            Swal.fire(
              "Successfully",
              "Your data has been "+ formData.status,
              "success"
            )
            this.ngOnInit()
          },
          err => {
            console.log(err)
          }
        )
      }
    });

  }

}
