import { Component, OnInit } from '@angular/core';
import { GetService } from '../../../../../core/get.service';
import { PatchService } from '../../../../../core/patch.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-holiday-list',
  templateUrl: './holiday-list.component.html',
  styleUrls: ['./holiday-list.component.css']
})
export class HolidayListComponent implements OnInit {

  holidayList;

  constructor(
    private _get: GetService,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this._get.getAllHoliday()
    .subscribe( data => {this.holidayList = data} )
  }

  getCount(){
    if(this.holidayList){
      const active = this.holidayList.filter(e => {
        return e.status === "active"
      })

      const deleted = this.holidayList.filter(e => {
        return e.status === "deleted"
      })

      return "Active : "+active.length+" | Deleted : "+deleted.length
    }
  }

  onDelete(id, type, unit){
    const formData: any = {}
    formData.id = id
    formData.name = unit
    formData.person = localStorage.getItem('rsesu')
    if(type == 'delete'){
      formData.status = 'deleted'
      formData.msg = 'has deleted'
      formData.msg2 = 'Delete'
    }else if(type == 'activate'){
      formData.status = "active"
      formData.msg = "has activated"
      formData.msg2 = 'Activate'
    }

    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes"
    }).then((result) => {
      if (result.value) {
        this._patch.delHoliday(formData)
        .subscribe(
          res => {
            Swal.fire(
              "Successfully",
              "Your data has been "+ formData.status,
              "success"
            )
            this.ngOnInit()
          },
          err => {
            console.log(err)
          }
        )
      }
    });
  }

}
