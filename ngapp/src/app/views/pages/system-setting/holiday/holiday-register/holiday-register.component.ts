import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { GetService } from '../../../../../core/get.service'
import { PostService } from '../../../../../core/post.service'
import { PatchService } from '../../../../../core/patch.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-holiday-register',
  templateUrl: './holiday-register.component.html',
  styleUrls: ['./holiday-register.component.css']
})
export class HolidayRegisterComponent implements OnInit {

  myForm: FormGroup
  notFound = ""
  loading = false
  pageTitle
  arrData:any = {}

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _post: PostService,
    private _get: GetService,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this.initForm()
    this.activatedRoute.params.subscribe(params => {
      const id = params.id
      if(id){
        this.pageTitle= "Edit"
        this._get.getSingleHoliday(id)
        .subscribe(
          res => {
            this.arrData = res
            this.arrData.id = id
            if(this.arrData.notFound === 1){
              //display no data found
              this.loading = true
              this.notFound = "Holiday ID is not found"
            }else{
              this.myForm = this.fb.group({
                holidayDate: [this.arrData.date, [Validators.required]],
                holidayTitle: [this.arrData.title, [Validators.required]]
              })
            }
          }
        )
      }else{
        this.pageTitle= "New"
        this.arrData.id = ''
      }
    })
  }

  initForm(){
    this.myForm = this.fb.group({
      holidayDate: ['', [Validators.required]],
      holidayTitle: ['', [Validators.required]]
    })
  }

  onSubmit(){
    this.loading = true
    if(this.arrData.id != ''){
      this.onPatch()
    }else{
      this.onRegister()
    }
  }

  onPatch(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    formData.id = this.arrData.id
    console.log(formData)
    this._patch.editHoliday(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: formData.holidayTitle + " successfully updated",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          setTimeout(() =>this.router.navigate(['/system/s/system/holiday-list']) ,500)
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }

  onRegister(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    this._post.postHoliday(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: res.title + " successfully registered",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          this.initForm()
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )

  }

  get holidayDate(){ return this.myForm.get('holidayDate') }
  get holidayTitle(){ return this.myForm.get('holidayTitle') }

}
