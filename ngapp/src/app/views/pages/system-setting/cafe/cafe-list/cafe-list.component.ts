import { Component, OnInit } from '@angular/core';
import { GetService } from '../../../../../core/get.service';
import { PatchService } from '../../../../../core/patch.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-cafe-list',
  templateUrl: './cafe-list.component.html',
  styleUrls: ['./cafe-list.component.css']
})
export class CafeListComponent implements OnInit {

  cafeList;

  constructor(
    private _get: GetService,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this._get.getAllCafe()
    .subscribe( data => this.cafeList = data )
  }

  getCount(){
    if(this.cafeList){
      const active = this.cafeList.filter(e => { return e.status === "active" })

      const deleted = this.cafeList.filter(e => { return e.status === "deleted" })

      return "Active : "+active.length+" | Deleted : "+deleted.length
    }
  }

  onDelete(id, type, unit){
    const formData: any = {}
    formData.id = id
    formData.name = unit
    formData.person = localStorage.getItem('rsesu')
    if(type == 'delete'){
      formData.status = 'deleted'
      formData.msg = 'has deleted'
    }else if(type == 'activate'){
      formData.status = "active"
      formData.msg = "has activated"
    }

    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes"
    }).then((result) => {
      if (result.value) {
        this._patch.delCafe(formData)
        .subscribe(
          res => {
            Swal.fire(
              "Successfully",
              "Your data has been "+ formData.status,
              "success"
            )
            this.ngOnInit()
          },
          err => {
            console.log(err)
          }
        )
      }
    });
  }

}
