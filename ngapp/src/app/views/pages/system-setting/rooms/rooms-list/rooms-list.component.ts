import { Component, OnInit } from '@angular/core';
import { GetService } from '../../../../../core/get.service';
import { PatchService } from '../../../../../core/patch.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.css']
})
export class RoomsListComponent implements OnInit {

  roomList;

  constructor( private _get : GetService, private _patch : PatchService ) { }

  ngOnInit(): void {
    this._get.getAllRoom()
    .subscribe( data => this.roomList = data )
  }

  getCount(){
    if(this.roomList){
      const active = this.roomList.filter(e => {
        return e.status === "active"
      })

      const deleted = this.roomList.filter(e => {
        return e.status === "deleted"
      })

      return "Active : "+active.length+" | Deleted : "+deleted.length
    }
  }

  onDelete(id, status, name){
    const roomData: any = {}
    roomData.id = id
    roomData.name = name
    roomData.person = localStorage.getItem('rsesu')
    if(status == 'delete'){
      roomData.status = "deleted"
      roomData.msg = "has deleted"
    }else if(status == 'activate'){
      roomData.status = "active"
      roomData.msg = "has activated"
    }
    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes"
    }).then((result) => {
      if (result.value) {
        this._patch.delRoom(roomData)
        .subscribe(
          res => {
            Swal.fire(
              roomData.msg,
              "Your data has been "+ roomData.status,
              "success"
            )
            this.ngOnInit()
          },
          err => {
            console.log(err)
          }
        )
      }
    });
  }

}
