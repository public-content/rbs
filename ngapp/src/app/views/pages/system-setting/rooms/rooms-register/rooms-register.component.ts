import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PostService } from '../../../../../core/post.service'
import { GetService } from '../../../../../core/get.service'
import { PatchService } from '../../../../../core/patch.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-rooms-register',
  templateUrl: './rooms-register.component.html',
  styleUrls: ['./rooms-register.component.css']
})
export class RoomsRegisterComponent implements OnInit {

  myForm: FormGroup;
  loading = false;
  pageTitle;
  notFound = ""
  arrData:any={};

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _post: PostService,
    private _get: GetService,
    private _patch: PatchService
    ) { }

  ngOnInit(): void {
    this.formInit()
    this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      if(id){
        this.pageTitle= "Edit"
        this._get.getSingleRoom(id)
        .subscribe(
          res => {
            this.arrData = res
            this.arrData.id = id
            if(this.arrData.notFound === 1){
              //display no data found
              this.loading = true
              this.notFound = "Room ID is not found"
            }else{
              this.myForm = this.fb.group({
                roomName : [this.arrData.roomName, [Validators.required]],
                location : [this.arrData.location, [Validators.required]],
                capacity : [this.arrData.capacity, [Validators.required]],
                microphone : [this.arrData.microphone],
                audio : [this.arrData.audio],
                lcd : [this.arrData.lcd]
              })
            }
          }

        )
      }else{
        this.pageTitle= "Register new"
        this.arrData.id = ''
      }
    })

  }

  formInit(){
    this.myForm = this.fb.group({
      roomName : ['', [Validators.required]],
      location : ['', [Validators.required]],
      capacity : ['', [Validators.required]],
      microphone : [false],
      audio : [false],
      lcd : [false]
    })
  }

  onSubmit(){
    this.loading = true
    if(this.arrData.id != ''){
      this.onPatch()
    }else{
      this.onRegister()
    }
  }

  onRegister(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    this._post.postRoom(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: res.roomName + " successfully registered",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          this.formInit()
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }

  onPatch(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    formData.id = this.arrData.id
    this._patch.editRoom(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: formData.roomName + " successfully updated",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          setTimeout(() =>this.router.navigate(['/system/s/system/room-list']) ,500)
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }

  get roomName(){ return this.myForm.get('roomName') }
  get location(){ return this.myForm.get('location') }
  get capacity(){ return this.myForm.get('capacity') }

}
