import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { SystemSettingComponent } from './system-setting.component';
import { CafeListComponent } from './cafe/cafe-list/cafe-list.component';
import { CafeRegisterComponent } from './cafe/cafe-register/cafe-register.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { DepartmentRegisterComponent } from './department/department-register/department-register.component';
import { HolidayListComponent } from './holiday/holiday-list/holiday-list.component';
import { HolidayRegisterComponent } from './holiday/holiday-register/holiday-register.component';
import { RoomsListComponent } from './rooms/rooms-list/rooms-list.component';
import { RoomsRegisterComponent } from './rooms/rooms-register/rooms-register.component';
import { TableListComponent } from './table/table-list/table-list.component';
import { TableRegisterComponent } from './table/table-register/table-register.component';

const routes: Routes = [
  {
    path: 'room-list',
    component: RoomsListComponent
  },
  {
    path: 'room-register',
    component: RoomsRegisterComponent
  },
  {
    path: 'room-register/:id',
    component: RoomsRegisterComponent
  },
  {
    path: 'department-list',
    component: DepartmentListComponent
  },
  {
    path: 'department-register',
    component: DepartmentRegisterComponent
  },
  {
    path: 'department-register/:id',
    component: DepartmentRegisterComponent
  },
  {
    path: 'table-list',
    component: TableListComponent
  },
  {
    path: 'table-register',
    component: TableRegisterComponent
  },
  {
    path: 'table-register/:id',
    component: TableRegisterComponent
  },
  {
    path: 'cafe-list',
    component: CafeListComponent
  },
  {
    path: 'cafe-register',
    component: CafeRegisterComponent
  },
  {
    path: 'cafe-register/:id',
    component: CafeRegisterComponent
  },
  {
    path: 'holiday-list',
    component: HolidayListComponent
  },
  {
    path: 'holiday-register',
    component: HolidayRegisterComponent
  },
  {
    path: 'holiday-register/:id',
    component: HolidayRegisterComponent
  }
]

@NgModule({
  declarations: [SystemSettingComponent, CafeListComponent, CafeRegisterComponent, DepartmentListComponent, DepartmentRegisterComponent, HolidayListComponent, HolidayRegisterComponent, RoomsListComponent, RoomsRegisterComponent, TableListComponent, TableRegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [ DatePipe ]
})
export class SystemSettingModule { }
