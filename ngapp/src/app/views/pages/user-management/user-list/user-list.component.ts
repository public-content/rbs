import { Component, OnInit } from '@angular/core';
import { GetService } from '../../../../core/get.service';
import { PatchService } from '../../../../core/patch.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList;

  constructor(
    private _get: GetService,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this._get.getAllUser().subscribe(data => this.userList = data)
  }

  getIcon(str){
    let capital = str.substring(0, 1)
    return capital
  }

  onDelete(id, type, unit){
    const formData: any = {}
    formData.id = id
    formData.name = unit
    formData.person = localStorage.getItem('rsesu')
    if(type == 'delete'){
      formData.status = 'deleted'
      formData.msg = 'has deleted'
    }else if(type == 'activate'){
      formData.status = "active"
      formData.msg = "has activated"
    }

    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes"
    }).then((result) => {
      if (result.value) {
        this._patch.delUser(formData)
        .subscribe(
          res => {
            Swal.fire(
              "Successfully",
              "Your data has been "+ formData.status,
              "success"
            )
            this.ngOnInit()
          },
          err => {
            console.log(err)
          }
        )
      }
    });
  }

}
