import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { GetService } from '../../../../core/get.service'
import { PostService } from '../../../../core/post.service'
import { PatchService } from '../../../../core/patch.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  myForm: FormGroup
  notFound = ""
  loading = false
  pageTitle
  arrData:any = {}
  departmentLists;
  usernameLock = false
  timer;
  pending = false
  userMsg = "the username must more than 3 character."



  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _post: PostService,
    private _get: GetService,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this._get.getActiveDepartment() .subscribe(data =>  this.departmentLists = data )
    this.formInit()
    this.activatedRoute.params.subscribe(params => {
      const id = params.id
      if(id){
        this.pageTitle= "Edit"
        this._get.getSingleUser(id)
        .subscribe(
          res => {
            this.arrData = res
            this.arrData.id = id
            if(this.arrData.notFound === 1){
              //display no data found
              this.loading = true
              this.notFound = "User ID is not found"
            }else{
              this.myForm = this.fb.group({
                fullname: [this.arrData.fullname, [Validators.required]],
                nickname: [this.arrData.nickname, [Validators.required]],
                department: [this.arrData.department, [Validators.required]],
                email: [this.arrData.email, [Validators.required, Validators.email]],
                designation: [this.arrData.designation, [Validators.required]],
                username: [this.arrData.username, [Validators.required, Validators.minLength(4)]],
                password: [this.arrData.password, [Validators.required]],
                userLevel: [this.arrData.userLevel, [Validators.required]]
              })
            }
          }
        )
      }else{
        this.pageTitle= "Register new"
        this.arrData.id = ''
      }
    })
  }

  formInit(){
    this.myForm = this.fb.group({
      fullname: ['', [Validators.required]],
      nickname: ['', [Validators.required]],
      department: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      designation: ['', [Validators.required]],
      username: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required]],
      userLevel: [[Validators.required]]
    })
  }

  usernameControl(){
    if(this.usernameLock == false){
      this.pending = true
      const userName:any = {}
      userName.username = this.myForm.get('username').value
      clearTimeout(this.timer)
      this.timer = setTimeout(()=>{
        this._post.checkUsernameService(userName)
        .subscribe(
          res => {
            const result = res.msg
            if(result == "available"){
            this.pending = false
            this.userMsg = "the username must more than 3 character."
            }else{
              this.userMsg = "this username has been used"
            }
          },
          err => { console.log(err) }
        )
      }, 500)
    }
  }

  onSubmit(){
    this.loading = true
    if(this.arrData.id != ''){
      this.onPatch()
    }else{
      this.onRegister()
    }
  }

  onRegister(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    this._post.postUser(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: res.fullname + " successfully registered",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          this.formInit()
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }

  onPatch(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    formData.id = this.arrData.id
    formData.passwordOld = this.arrData.password
    this._patch.editUser(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: formData.fullname + " successfully updated",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          setTimeout(() =>this.router.navigate(['/system/s/user/']) ,500)
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }



  get fullname() { return this.myForm.get('fullname') }
  get nickname() { return this.myForm.get('nickname') }
  get department() { return this.myForm.get('department') }
  get designation() { return this.myForm.get('designation') }
  get email() { return this.myForm.get('email') }
  get username() { return this.myForm.get('username') }
  get password() { return this.myForm.get('password') }
  get userLevel() { return this.myForm.get('userLevel') }
}
