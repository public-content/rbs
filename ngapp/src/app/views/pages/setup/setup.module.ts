import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list'
import interactionPlugin from '@fullcalendar/interaction'; // a plugin

import { SetupComponent } from './setup.component';
import { OncallComponent } from './oncall/oncall.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'oncall',
    pathMatch: 'full'
  },
  {
    path: 'oncall',
    component: OncallComponent
  }
];


FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,timeGridPlugin,listPlugin,interactionPlugin
]);

@NgModule({
  declarations: [SetupComponent, OncallComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    FullCalendarModule
  ],
  providers: [ DatePipe ]
})
export class SetupModule { }
