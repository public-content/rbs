import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { GetService } from '../../../../core/get.service'
import { PostService } from '../../../../core/post.service'
import { PatchService } from '../../../../core/patch.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-oncall',
  templateUrl: './oncall.component.html',
  styleUrls: ['./oncall.component.css']
})
export class OncallComponent implements OnInit {

  myForm: FormGroup
  fullcalendar: CalendarOptions
  testModal = false
  mdlSampleIsOpen : boolean = false;
  dummyData : any = []
  oncallArray;
  filterArray;

  constructor(
    private fb: FormBuilder,
    private _post: PostService,
    private _get: GetService,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this.formInit("")
    this.listInit()
  }

  async clickAction(arg) {

    this.filterArray = await this.oncallArray.filter(d => {
      return d.date === arg.dateStr
    })

    if(this.filterArray.length > 0){
      // this.filterArray = await this.filterArray[0]
      this.formInit(this.filterArray[0])
    }else{
      this.dummyData.date = arg.dateStr
      this.dummyData.oncall = ""
      this.dummyData.led = ""
      this.formInit(this.dummyData)
    }



    let element: HTMLElement = document.getElementById('btnModal') as HTMLElement;
    element.click();
  }

  formInit(dmdata){
    this.myForm = this.fb.group({
      date : [dmdata.date],
      oncall : [dmdata.oncall, [Validators.required]],
      led : [dmdata.led, [Validators.required]]
    })
  }

  listInit(){
    this._get.getOncallList()
    .subscribe(
      res => {
        this.oncallArray = res
        this.calendarFormat(this.oncallArray)
      },
      err => {
        console.log(err)
      }
    )
  }

  onSubmit(){
    if(this.filterArray.length > 0){
      let submitData = this.myForm.value
      if(submitData.oncall == this.filterArray[0].oncall && submitData.led == this.filterArray[0].led){
        console.log("Nothing Change")
      }else{
        submitData.person = localStorage.getItem('rsesu')
        submitData.id = this.filterArray[0]._id
        this._patch.editOncall(submitData)
        .subscribe(
          res => {
            this.listInit()
            let element: HTMLElement = document.getElementById('btnModal') as HTMLElement;
            element.click();
          },
          err => {
            Swal.fire({
              title: "Ohh no!!",
              text: err.message,
              icon: "warning",
              confirmButtonText: "Ok"
            }).then(()=> {
              console.log(err)
            })
          }
        )
      }
    }else{
      let submitData = this.myForm.value
      submitData.person = localStorage.getItem('rsesu')
      this._post.postOncall(submitData)
      .subscribe(
        res => {
          this.listInit()
          let element: HTMLElement = document.getElementById('btnModal') as HTMLElement;
          element.click();
        },
        err => {
          Swal.fire({
            title: "Ohh no!!",
            text: err.message,
            icon: "warning",
            confirmButtonText: "Ok"
          }).then(()=> {
            console.log(err)
          })
        }
      )
    }
  }

  calendarFormat(data){
    this.fullcalendar = {
      dayMaxEvents: true, // allow "more" link when too many events
      headerToolbar: {
        left: 'today',
        center: 'title',
        right: 'prev next'
      },
      eventOrder: 'allDay,start',
      dateClick: this.clickAction.bind(this),
      events:
        data
      ,
      eventTimeFormat: {
        hour: 'numeric',
        minute: '2-digit',
        meridiem: 'short'
      },
    }
  }

}
