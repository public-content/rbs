import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { ReportComponent } from './report.component';
import { GeneralComponent } from './general/general.component';
import { LoginComponent } from './login/login.component';
import { BookingComponent } from './booking/booking.component';
import { DeleteComponent } from './delete/delete.component';
import { UpdateComponent } from './update/update.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {
    path: 'general',
    component: GeneralComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'booking',
    component: BookingComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'update',
    component: UpdateComponent
  },
  {
    path: 'delete',
    component: DeleteComponent
  }
]

@NgModule({
  declarations: [ReportComponent, GeneralComponent, LoginComponent, BookingComponent, DeleteComponent, UpdateComponent, RegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [ DatePipe ]
})
export class ReportModule { }
