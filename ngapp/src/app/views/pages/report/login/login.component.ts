import { Component, OnInit } from '@angular/core';
import { GetService } from '../../../../core/get.service';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  lazy = true
  loader = false
  generalList:any = [
    {data1:"IT Awesome Yahooo Sting aku !!"},{data1:"IT Awesome Yahooo Sting aku !!"},{data1:"IT Awesome Yahooo Sting aku !!"},
    {data1:"IT Awesome Yahooo Sting aku !!"},{data1:"IT Awesome Yahooo Sting aku !!"},{data1:"IT Awesome Yahooo Sting aku !!"},
    {data1:"IT Awesome Yahooo Sting aku !!"},{data1:"IT Awesome Yahooo Sting aku !!"},{data1:"IT Awesome Yahooo Sting aku !!"},
    {data1:"IT Awesome Yahooo Sting aku !!"},{data1:"IT Awesome Yahooo Sting aku !!"},{data1:"IT Awesome Yahooo Sting aku !!"},
    {data1:"IT Awesome Yahooo Sting aku !!"},
  ]

  constructor(
    private _get: GetService,
    private datepipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.listInit()
  }

  listInit(){
    this._get.getAllTypeReport("Log")
    .subscribe(
      res => {
        this.generalList = []
        this.generalList = res
        if(this.generalList.length > 0){
          this.loader = false
          this.lazy = false
        }else{
          this.loader = true
          this.lazy = false
        }
      },
      err => { console.log(err) }
    )
  }

  convertDate(val){
    return this.datepipe.transform(val, 'dd MMM yyyy HH:mm:ss')
  }

}
