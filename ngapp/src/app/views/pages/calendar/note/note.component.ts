import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common'
import { GetService } from '../../../../core/get.service'
import { PatchService } from '../../../../core/patch.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  roomList: any = []
  loanList: any = []

  constructor(
    private datepipe: DatePipe,
    private _get : GetService,
    private _patch : PatchService
  ) { }

  ngOnInit(): void {
    this.getListLoan()
    this.getListRoom()
  }

  async getListRoom(){
    // this.roomList = [
    //   {
    //     dateStart : 'String',
    //     dateEnd : 'String',
    //     room : 'String',
    //     event : 'String',
    //     person : 'String',
    //     department : 'String',
    //     pax : 'String',
    //     remark : 'String',
    //     created : '',
    //     status : 'String'
    //   }
    // ]

    await this._get.getITRoom().subscribe(
      data => { this.roomList = data }
    )
  }

  async getListLoan(){
    // this.loanList = [
    //   {
    //     dateBorrow : 'String',
    //     dateReturn : 'String',
    //     reqLCD : 'String',
    //     reqNB : 'String',
    //     codeLCD : 'String',
    //     codeNB : 'String',
    //     person : 'String',
    //     department : 'String',
    //     event : 'String',
    //     location : 'String',
    //     remark : 'String',
    //     created : '',
    //     status : 'String'
    //   }
    // ]

    await this._get.getITLoan().subscribe(
      data => { this.loanList = data }
    )
  }

  convertDate(date,type){
    if(type == 'date'){
      let latest_date =this.datepipe.transform(date, 'yyyy-MM-dd');
      return latest_date
    }else if(type == 'time'){
      let latest_date =this.datepipe.transform(date, 'h:mm:ss a');
      return latest_date
    }
  }

  getLength(type){
    if(this.roomList && type == "room"){
      return this.roomList.length
    }else if(this.loanList && type == "loan"){
      return this.loanList.length
    }
  }

  onEnd(id, eventName){
    const formData: any = {}
    formData.id = id
    formData.event = eventName
    formData.person = localStorage.getItem('rsesu')
    Swal.fire({
      title: "Sure?",
      text: "Do you want to mark this as end",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes!"
    }).then((result) => {
        if (result.value) {
            this._patch.endRoom(formData)
            .subscribe(
              res => {
                Swal.fire(
                  "Success!",
                  "IT Training Event has ended.",
                  "success"
                )
                this.ngOnInit()
              },
              err =>{ console.log(err) }
            )
        }
    });
  }

  onReturn(id, eventName){
    const formData: any = {}
    formData.id = id
    formData.event = eventName
    formData.person = localStorage.getItem('rsesu')
    Swal.fire({
      title: "Sure?",
      text: "Do you want to mark this as return",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes!"
    }).then((result) => {
        if (result.value) {
            this._patch.returnLoan(formData)
            .subscribe(
              res => {
                Swal.fire(
                  "Success!",
                  "Loan Assets has been returned.",
                  "success"
                )
                this.ngOnInit()
              },
              err =>{ console.log(err) }
            )
        }
    });
  }

}
