import { Component, OnInit, ɵConsole } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import { GetService } from '../../../../core/get.service'
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  arrServer:any = [];
  arrTemp;
  singleEventArray:any = [];
  modalArray:any = [];
  alertArray:any = [];
  nextRefreshTime;
  loading = false
  showModal = false
  fullcalendar: CalendarOptions
  filtering = "All";
  constructor( private _get : GetService, private datepipe: DatePipe ) { }

  ngOnInit(): void {
    this.fetchServer()
    this.nextRefresh()
  }

  ngOnRefresh(filt){
    this.fetchServerRefresh(filt)
    this.nextRefresh()
  }

  onFilter(event : any){
    this.filtering = event.target.value
    this.arrTemp = this.arrServer
    if(this.filtering == 'All'){
      this.calendarFormat(this.arrTemp)
    }else{
      this.arrTemp = this.arrTemp.filter( arg => { return arg.dept.includes(this.filtering) || arg.type == "holiday" || arg.type == "oncall" } )
      this.calendarFormat(this.arrTemp)
    }
  }

  onFilterRefresh(filt){
    this.filtering = filt
    this.arrTemp = this.arrServer
    if(this.filtering == 'All' || this.filtering == 'undefined'){
      this.calendarFormat(this.arrTemp)
    }else{
      this.arrTemp = this.arrTemp.filter( arg => { return arg.dept.includes(this.filtering) || arg.type == "holiday" || arg.type == "oncall" } )
      this.calendarFormat(this.arrTemp)
    }
  }

  async fetchServer(){
    this.loading = true
    this._get.getAllEvents().subscribe(
      res => {
        this.loading = false
        this.arrServer = res
        // console.log("array is set")
        this.calendarFormat(this.arrServer)
      }
    )
  }

  async fetchServerRefresh(filt){
    this.loading = true
    this._get.getAllEvents().subscribe(
      res => {
        this.loading = false
        this.arrServer = res
        this.onFilterRefresh(filt)
      }
    )
  }


  calendarFormat(data){
    this.fullcalendar = {
      //dayMaxEvents: true, // allow "more" link when too many events
      headerToolbar: {
        left: 'today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,listWeek prev,next'
      },
      eventOrder: 'allDay,start',
      // events: "http://localhost:3000/get/active/masterEvents",
      eventClick: this.clickAction.bind(this),
      events:
        data
      ,
      eventTimeFormat: {
        hour: 'numeric',
        minute: '2-digit',
        meridiem: 'short'
      },
    }

    // console.log("Calendar format set")
  }

  clickAction(arg){
    if(arg.event.extendedProps.type == "booking"){
      let element: HTMLElement = document.getElementById('btnModalEvent') as HTMLElement;
      element.click();
      this.showModal = true
      this._get.getOncallSingle(arg.event.extendedProps._id)
      .subscribe(
        res => {
            this.singleEventArray = res
        },
        err => {
          console.log(err)
        }
      )
    }
  }

  nextRefresh(){
    let sec = 60000
    let now = new Date()
    let nAfter = now.getTime() + sec
    let display = this.datepipe.transform(nAfter, 'h:mm:ss a')
    this.nextRefreshTime = display
    setTimeout(() => { this.ngOnRefresh(this.filtering) }, sec)

    if(this.arrServer.length > 0){
      // let nowa = "2020-11-03T14:30"
      let nowTime = this.datepipe.transform((now.getTime() + 1800000), 'yyyy-MM-ddTHH:mm')
      let tempData = this.arrServer
      tempData = tempData.filter( arg => { return arg.start == nowTime && arg.type == "booking" } )
      // console.log(now)
      // console.log(nowTime)
      // console.log(tempData)
      if(tempData.length > 0){
        // we will send temp to two diffrent method
        this.alertToast(tempData)
        this.modalToast(nowTime)
      }
    }
  }

  alertToast(data){
    this.alertArray = this.alertArray.concat(data)
    // console.log(data)
  }

  modalToast(dateTime){
    let dateOnly = this.datepipe.transform(dateTime, 'yyyy-MM-dd')
    this._get.getEventInfoFilter(dateOnly).subscribe(
      res => {
        this.modalArray = res
        this.modalArray = this.modalArray.filter( arg => { return arg.startDate == dateTime } )
        if(this.modalArray.length > 0){
          this.singleEventArray = this.singleEventArray.concat(this.modalArray)
          if(this.showModal == false){
            let element: HTMLElement = document.getElementById('btnModalEvent') as HTMLElement;
            element.click();
            this.showModal = true
          }
        }
      }
    )
  }

  clearModal(){
    setTimeout(()=>{
      this.showModal = false,
      this.singleEventArray = []
    }, 500)
  }

  clearAlert(){
    this.alertArray = []
  }

  convertMinute(value){
    let timeStampsValue = new Date(value)
    let timeStampsNow = new Date()
    let diff =(timeStampsValue.getTime() - timeStampsNow.getTime() ) / 1000;
    diff /= (60);
    return Math.round(diff) + " Minutes"
  }

  OnlyIT(){
    if (localStorage.getItem("detail") === null) {

    }else{
      let userDepartment = JSON.parse(localStorage.getItem('detail')).department
      if(userDepartment == "Information Technology"){
        return true
      }else{
        return false
      }
    }
  }

}
