import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list'
import interactionPlugin from '@fullcalendar/interaction'; // a plugin

import { CalendarComponent } from './calendar.component';
import { BodyComponent } from './body/body.component';
import { NoteComponent } from './note/note.component';
import { EventInfoComponent } from './event-info/event-info.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'body',
    pathMatch: 'full'
  },
  {
    path: 'body',
    component: BodyComponent
  }
];

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,timeGridPlugin,listPlugin,interactionPlugin
]);

@NgModule({
  declarations: [CalendarComponent, BodyComponent, NoteComponent, EventInfoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FullCalendarModule
  ]
})
export class CalendarModule { }
