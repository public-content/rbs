import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../../core/auth.service'
import { HttpClient  } from '@angular/common/http';
import { GetService } from '../../../../core/get.service'
import Swal from 'sweetalert2'
import { detect } from 'detect-browser'
const browser = detect();

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;
  loading = false;
  failure = false;
  msg = '';
  showAlertBrowser = false
  ipAddress: any = '';

  constructor(
    private fb: FormBuilder,
    private _router : Router,
    private _auth : AuthService,
    private http:HttpClient,
    private _get:GetService
  ) { }

  ngOnInit(): void {
    this._auth.readyLoggedIn()
    this.initLoginForm()
    this.getIPAddress();
    if(browser.name === 'firefox'){
      this.showAlertBrowser = true
    }
  }

  getIPAddress(){
    // this.http.get("http://api.ipify.org/?format=json").subscribe(
    //   res => { this.ipAddress = res, console.log(this.ipAddress) }
    // );
    // this.http.get('https://jsonip.com/')
    // .subscribe(
    //   (res:any) => {
    //     this.ipAddress = res.ip
    //   },
    //   (err) => {
    //     console.log(err)
    //   }
    // )
    this._get.getIpAddress().subscribe( res => { this.ipAddress = res }, err => { console.log(err) } )
  }

  initLoginForm(){
    this.myForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required]]
    })
    this.failure = false;
    this.msg = '';
  }

  onSubmit(){
    const formData = this.myForm.value
    this._auth.postAuth(formData).subscribe(
      res => {
        let detailUser = res.user
        localStorage.setItem('token', res.token),
        localStorage.setItem('rsesu', res.nickname),
        localStorage.setItem('detail', JSON.stringify(detailUser)),
        Swal.fire({
          title: "Successfully",
          text: "Login to the system",
          icon: "success",
          confirmButtonText: "Ok"
      }).then((result) => {
          if (result.value) {
              Swal.fire(
                  "Welcome back!",
                  "Room Booking System"
              )
              window.location.replace('sys')
          }
      });
      console.log(detailUser)
      },
      err => { this.failure=true, this.msg=err.error }
    )
  }

  get username() { return this.myForm.get('username') }
  get password() { return this.myForm.get('password') }

}
