import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { PostService } from '../../../../../core/post.service'
import { PatchService } from '../../../../../core/patch.service'
import { GetService } from '../../../../../core/get.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-room-training-register',
  templateUrl: './room-training-register.component.html',
  styleUrls: ['./room-training-register.component.css']
})
export class RoomTrainingRegisterComponent implements OnInit {

  myForm: FormGroup
  departmentLists
  loading = false


  constructor(
    private fb: FormBuilder,
    private _post : PostService,
    private _patch : PatchService,
    private _get : GetService,
    private actRoute : ActivatedRoute,
    private _route : Router
  ) { }

  ngOnInit(): void {
    this._get.getActiveDepartment().subscribe( data =>  this.departmentLists = data )
    this.formInit()
  }

  formInit(){
    this.myForm = this.fb.group({
      dateStart : ['', [Validators.required]],
      dateEnd : ['', [Validators.required]],
      room : ['IT Training Room', [Validators.required]],
      event : ['', [Validators.required]],
      person : ['', [Validators.required]],
      department : ['', [Validators.required]],
      pax : [''],
      remark : [''],
    })
  }

  onSubmit(){
    const formData = this.myForm.value
    formData.person1 = localStorage.getItem('rsesu')
    this._post.postITRoom(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: res.event + " successfully registered",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          this.formInit()
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }

  get dateStart (){ return this.myForm.get('dateStart') }
  get dateEnd (){ return this.myForm.get('dateEnd') }
  get room (){ return this.myForm.get('room') }
  get event (){ return this.myForm.get('event') }
  get person (){ return this.myForm.get('person') }
  get department (){ return this.myForm.get('department') }

}
