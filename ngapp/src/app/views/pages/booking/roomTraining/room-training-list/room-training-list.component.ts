import { Component, OnInit } from '@angular/core';
import { GetService } from '../../../../../core/get.service'
import { PatchService } from '../../../../../core/patch.service'
import { DatePipe } from '@angular/common'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-room-training-list',
  templateUrl: './room-training-list.component.html',
  styleUrls: ['./room-training-list.component.css']
})
export class RoomTrainingListComponent implements OnInit {

  roomList: any = []

  constructor(
    private datepipe: DatePipe,
    private _get : GetService,
    private _patch : PatchService
  ) { }

  ngOnInit(): void {
    this._get.getITRoom().subscribe( data => this.roomList = data  )
  }

  convertDate(date,type){
    if(type == 'date'){
      let latest_date =this.datepipe.transform(date, 'yyyy-MM-dd');
      return latest_date
    }else if(type == 'time'){
      let latest_date =this.datepipe.transform(date, 'h:mm:ss a');
      return latest_date
    }
  }

  getLength(type){
    if(this.roomList && type == "room"){
      return this.roomList.length
    }
  }

  onEnd(id, eventName){
    const formData: any = {}
    formData.id = id
    formData.event = eventName
    formData.person = localStorage.getItem('rsesu')
    Swal.fire({
      title: "Sure?",
      text: "Do you want to mark this as end",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes!"
    }).then((result) => {
        if (result.value) {
            this._patch.endRoom(formData)
            .subscribe(
              res => {
                Swal.fire(
                  "Success!",
                  "IT Training Event has ended.",
                  "success"
                )
                this.ngOnInit()
              },
              err =>{ console.log(err) }
            )
        }
    });
  }

}
