import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common'
import { GetService } from '../../../../../core/get.service'
import { PatchService } from '../../../../../core/patch.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-room-event-list',
  templateUrl: './room-event-list.component.html',
  styleUrls: ['./room-event-list.component.css']
})
export class RoomEventListComponent implements OnInit {


  today = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
  title = "List of Room Booking";
  singleEventArray;
  arrEvent;
  tempDate;
  historyArray;

  constructor(
    private datepipe: DatePipe,
    private _get : GetService,
    private _patch : PatchService
  ) { }

  ngOnInit(): void {
    this.arrayEvent(this.today)
  }

  changeDate(valuee : any){
    this.tempDate = valuee.target.value
    this.arrayEvent(this.tempDate)
    console.log(this.tempDate)
  }

  arrayEvent(date){
    this._get.getEventInfoFilter(date).subscribe(
      res => {
        this.arrEvent = res
        // console.log(this.arrEvent)
        }
    )
  }

  convertDate(date,type){
    if(type == 'date'){
      let latest_date = this.datepipe.transform(date, 'yyyy-MM-dd');
      return latest_date
    }else if(type == 'time'){
      let latest_date = this.datepipe.transform(date, 'h:mm:ss a');
      return latest_date
    }else if(type == 'created'){
      let latest_date = this.datepipe.transform(date, 'EEE MMM d y h:mm:ss a');
      return latest_date
    }
  }

  pushEventID(id){
    this.singleEventArray = this.arrEvent.filter(arg => {
      return arg._id === id
    })
  }

  userCreated(id){
    let role = JSON.parse(localStorage.getItem('detail')).userLevel
    if(role == 'Super Administrator' || role == 'Administrator' || role == 'Admin'){
      return true
    }else{
      let person_id = JSON.parse(localStorage.getItem('detail'))._id
      if(person_id == id){
        return true
      }else{
        return false
      }
    }
  }

  historyButton(id){
    this._get.getSingleHistory(id).subscribe(
      res => {
        this.historyArray = res
        let element: HTMLElement = document.getElementById('historyModalListBtn') as HTMLElement;
        element.click();
      },
      err => { console.log(err) }
    )
  }

  onDelete(info, master, eventName){
    let data: any = {}
    data.infoID = info
    data.eventID = master
    data.eventName = eventName
    data.msg = " has been deleted by "
    data.status = "deleted"
    data.statusLog = "Delete"
    data.person = JSON.parse(localStorage.getItem('detail')).fullname

    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes"
    }).then((result) => {
      if (result.value) {
        this._patch.delEvent(data)
        .subscribe(
          res => {
            let subRes = res
            Swal.fire(
              "Successfully",
              subRes.msg,
              "success"
            )
            this.arrayEvent(this.tempDate)
          },
          err => {
            Swal.fire(
              "Ohh no!!",
              err,
              "warning"
            )
          }
        )
      }
    });


  }

  onActive(info, master, eventName){
    let data: any = {}
    data.infoID = info
    data.eventID = master
    data.eventName = eventName
    data.msg = " has been activated by "
    data.status = "active"
    data.statusLog = "Activate"
    data.person = JSON.parse(localStorage.getItem('detail')).fullname

    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes"
    }).then((result) => {
      if (result.value) {
        this._patch.delEvent(data)
        .subscribe(
          res => {
            let subRes = res
            Swal.fire(
              "Successfully",
              subRes.msg,
              "success"
            )
            this.arrayEvent(this.tempDate)
          },
          err => {
            Swal.fire(
              "Ohh no!!",
              err,
              "warning"
            )
          }
        )
      }
    });
  }

}
