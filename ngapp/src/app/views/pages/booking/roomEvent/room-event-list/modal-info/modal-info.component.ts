import { Component, OnInit, Input } from '@angular/core';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-modal-info',
  templateUrl: './modal-info.component.html',
  styleUrls: ['./modal-info.component.css']
})
export class ModalInfoComponent implements OnInit {

  constructor(
    private datepipe: DatePipe
  ) { }

  @Input() singleEventArray : [];

  ngOnInit(): void {

  }

  convertDate(date,type){
    if(date != ''){
      if(type == 'date1'){
        let latest_date = this.datepipe.transform(date, 'h:mm a  yyyy-MM-dd');
        return latest_date
      }else if(type == 'date2'){
        let latest_date = this.datepipe.transform(date, 'EEE MMM d y h:mm:ss a');
        return latest_date
      }
    }
  }

}
