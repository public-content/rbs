import { Component, OnInit, Input  } from '@angular/core';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-modal-history',
  templateUrl: './modal-history.component.html',
  styleUrls: ['./modal-history.component.css']
})
export class ModalHistoryComponent implements OnInit {

  arrEventHistorySingle;

  constructor(
    private datepipe: DatePipe
  ) { }

  @Input() historyArray :any [];

  ngOnInit(): void {

  }

  convertDate(date,type){
    if(type == 'date'){
      let latest_date = this.datepipe.transform(date, 'yyyy-MM-dd');
      return latest_date
    }else if(type == 'time'){
      let latest_date = this.datepipe.transform(date, 'h:mm:ss a');
      return latest_date
    }else if(type == 'created'){
      let latest_date = this.datepipe.transform(date, 'EEE MMM d y h:mm:ss a');
      return latest_date
    }else if(type == 'date1'){
      let latest_date = this.datepipe.transform(date, 'h:mm a  yyyy-MM-dd');
      return latest_date
    }else if(type == 'date2'){
      let latest_date = this.datepipe.transform(date, 'EEE MMM d y h:mm:ss a');
      return latest_date
    }
  }

  async pushEventID(id){
    this.arrEventHistorySingle = await this.historyArray.filter(arg => {
      return arg._id === id
    })
    let element: HTMLElement = document.getElementById('historyModalInfoBtn') as HTMLElement;
    element.click();
  }

}
