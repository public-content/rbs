import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { GetService } from '../../../../../core/get.service'
import { PostService } from '../../../../../core/post.service'
import { PatchService } from '../../../../../core/patch.service'
import { DatePipe } from '@angular/common'
import Swal from 'sweetalert2'


@Component({
  selector: 'app-room-event-register',
  templateUrl: './room-event-register.component.html',
  styleUrls: ['./room-event-register.component.css']
})
export class RoomEventRegisterComponent implements OnInit {

  myForm: FormGroup;
  todayDate: Date = new Date();
  listDepartment;
  listVenue;
  listSetup;
  listMenu;
  setupVenue;
  arrEvent;
  arrEditEvent:any = [];
  controlReqIT = false;
  controlReqCafe = false;
  controlReqPark = false;
  controlNeed = false;
  controlError = false;
  loading = false;
  msgError = "";
  notFound = "";
  cardTitle;
  minDate = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private _get : GetService,
    private _post : PostService,
    private _patch : PatchService,
    private datepipe: DatePipe
  ) { }


  ngOnInit(): void {
    this.minDate = this.datepipe.transform(Date.now(), "yyyy-MM-ddTHH:mm")
    this.getVenueList()
    this.getSetupList()
    this.getMenuList()
    this.initBookingForm()
    this._get.getActiveDepartment() .subscribe(data =>  this.listDepartment = data )
    this.activatedRoute.params.subscribe(params => {
      const id = params.id
      if(id){
        this.cardTitle = "Edit Event"
        this._get.getSingleEvent(id).subscribe(
          res => {
            this.loading = true
            this.arrEditEvent = res
            this.arrEditEvent.id = id
            if(this.arrEditEvent.notFound === 1 ){
              this.loading = true
              this.notFound = "Event ID is not found"
            }else{
              this.loadArrayData()
            }
          }
        )
      }else{
        this.cardTitle = "Register New Event"
        this.arrEditEvent.id = ""
      }
    })
  }

  loadData(){
    this.myForm = this.fb.group({
      startDate : [this.arrEditEvent.startDate, Validators.required],
      endDate : [this.arrEditEvent.endDate, Validators.required],
      venue : [this.arrEditEvent.venue, Validators.required],
      layout : [this.arrEditEvent.layout, Validators.required],
      remarkHK : [this.arrEditEvent.remarkHK],
      eventName : [this.arrEditEvent.eventName, Validators.required],
      noPax : [this.arrEditEvent.noPax, Validators.required],
      requestBy : [this.arrEditEvent.requestBy, Validators.required],
      requestDepartment : [this.arrEditEvent.requestDepartment, Validators.required],
      reqIT : [this.arrEditEvent.reqIT],
      reqCafe : [this.arrEditEvent.reqCafe],
      reqPark : [this.arrEditEvent.reqPark],
      needNB : [this.arrEditEvent.needNB],
      needMic : [this.arrEditEvent.needMic],
      needPointer : [this.arrEditEvent.needPointer],
      needLcd: [this.arrEditEvent.needLcd],
      needAudio : [this.arrEditEvent.needAudio],
      remarkIT : [this.arrEditEvent.remarkIT],
      cafeMenu : [this.arrEditEvent.cafeMenu],
      remarkCafe : [this.arrEditEvent.remarkCafe],
      noValet : [this.arrEditEvent.noValet],
      noNormal : [this.arrEditEvent.noNormal],
      remarkValet : [this.arrEditEvent.remarkValet],
      remarkNormal : [this.arrEditEvent.remarkNormal],
      remarkOverall : [this.arrEditEvent.remarkOverall]
    })
  }

  loadSetting(){
    this.controlNeed = true
    this.controlError = false
    this.controlReqIT = this.arrEditEvent.reqIT;
    this.controlReqCafe = this.arrEditEvent.reqCafe;
    this.controlReqPark = this.arrEditEvent.reqPark;
    this.getVenueDetails(this.arrEditEvent.venue)
  }

  async loadArrayData(){
    await this.loadData()
    await this.loadSetting()
    return this.loading = false
  }

  initBookingForm(){
    this.controlReqIT = false;
    this.controlReqCafe = false;
    this.controlReqPark = false;
    this.myForm = this.fb.group({
      startDate : ['', Validators.required],
      endDate : ['', Validators.required],
      venue : ['', Validators.required],
      layout : ['', Validators.required],
      remarkHK : [''],
      eventName : ['', Validators.required],
      noPax : ['', Validators.required],
      requestBy : ['', Validators.required],
      requestDepartment : ['', Validators.required],
      reqIT : [false],
      reqCafe : [false],
      reqPark : [false],
      needNB : [false],
      needMic : [false],
      needPointer : [false],
      needLcd: [false],
      needAudio : [false],
      remarkIT : [''],
      cafeMenu : [''],
      remarkCafe : [''],
      noValet : [''],
      noNormal : [''],
      remarkValet : [''],
      remarkNormal : [''],
      remarkOverall : ['']
    })
  }


  getLocalS(type){
    let nickname = localStorage.getItem('rsesu')
    return nickname
  }

  getControl(type){
    if(type == "IT"){
      const checkIT = this.myForm.get('reqIT').value
      if(checkIT == false){
        this.controlReqIT = true;
      }else{
        this.controlReqIT = false;
      }
    }else if(type == "CAFE"){
      const checkCafe = this.myForm.get('reqCafe').value
      if(checkCafe == false){
        this.controlReqCafe = true;
      }else{
        this.controlReqCafe = false;
      }
    }else if(type == "PR"){
      const checkPark = this.myForm.get('reqPark').value
      if(checkPark == false){
        this.controlReqPark = true;
      }else{
        this.controlReqPark = false;
      }
    }
    // console.log([this.controlReqIT, this.controlReqCafe, this.controlReqPark])
  }

  getVenueList(){ this._get.getActiveVenue().subscribe(data => { this.listVenue = data }) }
  getSetupList(){ this._get.getActiveSetup().subscribe(data => { this.listSetup = data }) }
  getMenuList(){ this._get.getActiveCafe().subscribe(data => { this.listMenu = data }) }
  getVenueDetails(venue){
    this.setupVenue = this.listVenue.filter(arg => {
      return arg.roomName === venue && arg.status === 'active'
    })
    // return console.log(this.setupVenue)
  }
  async getDayEvent(date){

  }

  async eventValidation(){
    //checking input value
    const inputStart = this.myForm.get('startDate').value
    const inputEnd = this.myForm.get('endDate').value
    const inputVenue = this.myForm.get('venue').value
    const inputLayout = this.myForm.get('layout').value
    // console.log(inputStart)
    //validate input
    if(inputStart && inputEnd && inputVenue){
      //get venue setup for IT
      this.getVenueDetails(inputVenue)
      //get array
      await this._get.getEventInfoFilter(inputStart)
      .subscribe(
        res => {
          this.arrEvent = res
          // console.log(["Array :", this.arrEvent]) //------------------------------------
          // make sure inputstart gap 1hr from inputEnd
            if(this.arrEvent.notFound === 1){
              //server return error
              console.log('server side problem')
            }else{
              if(this.getTimestamp(inputStart, 59) < this.getTimestamp(inputEnd, 0)){
                //check array length before proceed looping
                //filter according input venue
                if(this.arrEditEvent.id != ""){
                  const existID = this.arrEditEvent.id
                  this.arrEvent = this.arrEvent.filter(function(d){
                    return d._id != existID && d.venue === inputVenue && d.status === 'active'
                  })
                  // console.log(this.arrEvent)
                  // console.log(existID)
                }else{
                  this.arrEvent = this.arrEvent.filter(function(d){
                    return d.venue === inputVenue && d.status === 'active'
                  })
                  // console.log(this.arrEvent)
                }
                if(this.arrEvent.length > 0){
                  // console.log(["Array filter:", this.arrEvent]) //--------------------------
                  // loop array for validation using FOR loop
                  for (let i = 0; i < this.arrEvent.length; i++) {
                    if(this.getTimestamp(inputStart, 0) < this.getTimestamp(this.arrEvent[i].startDate, -15)){
                      if(i != 0){
                        if(this.getTimestamp(inputStart, 0) > this.getTimestamp(this.arrEvent[i-1].endDate, 15)){
                          if(this.getTimestamp(inputEnd, 0) < this.getTimestamp(this.arrEvent[i].startDate, -15)){
                            // return true
                            // console.log("Success pass all the conditon")
                            this.controlNeed = true
                            this.controlError = false
                          }else{
                            // return false
                            // console.log("Error: End Date clash with last  event: " + this.arrEvent[i].eventName)
                            this.controlError = true
                            this.controlNeed = false
                            this.msgError = "Error: End Date clash with last  event: " + this.arrEvent[i].eventName
                          }
                        }else{
                          // return false
                          // console.log("Error: Start Date clash with last event: " + this.arrEvent[i-1].eventName)
                          this.controlError = true
                          this.controlNeed = false
                          this.msgError = "Error: Start Date clash with last event: " + this.arrEvent[i-1].eventName
                        }
                      }else{
                        if(this.getTimestamp(inputEnd, 0) < this.getTimestamp(this.arrEvent[i].startDate, -15)){
                          // return true
                          // console.log("Success pass all the conditon")
                          this.controlNeed = true
                          this.controlError = false
                        }else{
                          // return false
                          // console.log("Error: End Date clash with next event: " + this.arrEvent[i].eventName)
                          this.controlError = true
                          this.controlNeed = false
                          this.msgError = "Error: End Date clash with next event: " + this.arrEvent[i].eventName
                        }
                      }
                      //end loop
                      i = this.arrEvent.length+1
                    }else{
                      // proceed plan b
                      if(i === (this.arrEvent.length-1)){
                        if(this.getTimestamp(inputStart, 0) > this.getTimestamp(this.arrEvent[i].endDate, 15)){
                          // return true
                          // console.log("Success pass all the conditon")
                          this.controlNeed = true
                          this.controlError = false
                        }else{
                          //not to my self : This will not support next day event, unless u call the next day event together
                          // return false
                          // console.log("Error: Start Date clash with last event: " + this.arrEvent[i].eventName)
                          this.controlError = true
                          this.controlNeed = false
                          this.msgError = "Error: Start Date clash with last event: " + this.arrEvent[i].eventName
                        }
                      }
                    }
                  }
                }else{
                  // console.log("Success condition not meet")
                  this.controlNeed = true
                  this.controlError = false
                }
              }else{
                // Error: Event gap must atleast 1 hour
                // console.log("Error: Event duration must atleast 1 hour")
                this.controlError = true
                this.controlNeed = false
                this.msgError = "Error: Event duration must atleast 1 hour"
              }
            }

        }
      )
    }
  }

  getTimestamp(time, min){
    return new Date(time).getTime() + min*60000
  }

  onSubmit(){
    this.loading = true
    if(this.arrEditEvent.id != ''){
      this.onPatch()
    }else{
      this.onRegister()
    }
  }

  onPatch(){
    let formData = this.myForm.value
    formData.person = localStorage.getItem('rsesu')
    formData.eventID = this.arrEditEvent.masterEvents
    formData.infoID = this.arrEditEvent.id
    this._patch.editEvent(formData)
    .subscribe(
      res => {
        let result = res
        if(result.status == "Yes"){
          Swal.fire({
            title: "Yeahh!!",
            text: result.msg,
            icon: "success",
            confirmButtonText: "Ok"
          }).then(()=> {
            this.loading = false
            // console.log(result)
            setTimeout(() =>this.router.navigate(['/system/s/booking/room-list']) ,500)
          })
        }else{
          Swal.fire({
            title: "Hmmm...",
            text: result.msg,
            icon: "success",
            confirmButtonText: "Ok"
          }).then(()=> {
            this.loading = false
            // console.log(result)
            setTimeout(() =>this.router.navigate(['/system/s/booking/room-list']) ,500)
          })
        }
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
    // console.log(this.arrEditEvent)
    // console.log(formData)
  }

  onRegister(){
    const formData = this.myForm.value
    formData.duration = this.calcDuration(formData.startDate, formData.endDate)
    formData.person = localStorage.getItem('rsesu')
    formData.person_id = JSON.parse(localStorage.getItem("detail"))._id
    this._post.postBooking(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: formData.eventName + " successfully registered",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          this.ngOnInit()
        })
        console.log(res)
      }
    )
    console.log(formData)
  }

  calcDuration(start, end){
        let date_future:any = new Date(end)
        let date_now:any = new Date(start)

        // let seconds = Math.floor((date_future - (date_now)));
        let seconds = Math.floor((date_future - (date_now))/1000);
        // let minutes = Math.floor(seconds/60);
        // let hours = Math.floor(minutes/60);
        // let days = Math.floor(hours/24);

        // hours = hours-(days*24);
        // minutes = minutes-(days*24*60)-(hours*60);
        // seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);

        // return in second
        return seconds
  }

  get startDate(){ return this.myForm.get('startDate') }
  get endDate(){ return this.myForm.get('endDate') }
  get venue(){ return this.myForm.get('venue') }
  get layout(){ return this.myForm.get('layout') }
  get eventName(){ return this.myForm.get('eventName') }
  get noPax(){ return this.myForm.get('noPax') }
  get requestBy(){ return this.myForm.get('requestBy')}
  get requestDepartment(){ return this.myForm.get('requestDepartment')}

}
