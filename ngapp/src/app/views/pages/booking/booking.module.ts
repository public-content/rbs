import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';


import { BookingComponent } from './booking.component';
import { RoomEventRegisterComponent } from './roomEvent/room-event-register/room-event-register.component';
import { RoomEventListComponent } from './roomEvent/room-event-list/room-event-list.component';
import { LoanRegisterComponent } from './loan/loan-register/loan-register.component';
import { LoanListComponent } from './loan/loan-list/loan-list.component';
import { RoomTrainingRegisterComponent } from './roomTraining/room-training-register/room-training-register.component';
import { RoomTrainingListComponent } from './roomTraining/room-training-list/room-training-list.component';
import { ModalInfoComponent } from './roomEvent/room-event-list/modal-info/modal-info.component';
import { ModalHistoryComponent } from './roomEvent/room-event-list/modal-history/modal-history.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'room-list',
    pathMatch: 'full'
  },
  {
    path: 'room-list',
    component: RoomEventListComponent
  },
  {
    path: 'room-register',
    component: RoomEventRegisterComponent
  },
  {
    path: 'room-register/:id',
    component: RoomEventRegisterComponent
  },
  {
    path: 'loan-list',
    component: LoanListComponent
  },
  {
    path: 'loan-register',
    component: LoanRegisterComponent
  },
  {
    path: 'it-list',
    component: RoomTrainingListComponent
  },
  {
    path: 'it-register',
    component: RoomTrainingRegisterComponent
  }
];

@NgModule({
  declarations: [BookingComponent, RoomEventRegisterComponent, RoomEventListComponent, LoanRegisterComponent, LoanListComponent, RoomTrainingRegisterComponent, RoomTrainingListComponent, ModalInfoComponent, ModalHistoryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [ DatePipe ]
})
export class BookingModule { }
