import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { PostService } from '../../../../../core/post.service'
import { PatchService } from '../../../../../core/patch.service'
import { GetService } from '../../../../../core/get.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-loan-register',
  templateUrl: './loan-register.component.html',
  styleUrls: ['./loan-register.component.css']
})
export class LoanRegisterComponent implements OnInit {

  myForm: FormGroup
  departmentLists
  loading = false
  showCodeLCD = false
  showCodeNB = false


  constructor(
    private fb: FormBuilder,
    private _post : PostService,
    private _patch : PatchService,
    private _get : GetService,
    private actRoute : ActivatedRoute,
    private _route : Router
  ) { }

  ngOnInit(): void {
    this._get.getActiveDepartment().subscribe( data =>  this.departmentLists = data )
    this.formInit()
  }

  formInit(){
    this.myForm = this.fb.group({
      dateBorrow : ['', [Validators.required]],
      dateReturn : ['', [Validators.required]],
      reqLCD : [false],
      reqNB : [false],
      codeLCD : ['IT - ', [Validators.required]],
      codeNB : ['IT - ', [Validators.required]],
      person : ['', [Validators.required]],
      department : ['', [Validators.required]],
      event : ['', [Validators.required]],
      location : ['', [Validators.required]],
      remark : [''],
    })
    this.showCodeLCD = this.myForm.get('reqLCD').value
    this.showCodeNB = this.myForm.get('reqNB').value
  }

  showCode(type){
    if(type == 'LCD'){
      this.showCodeLCD = this.myForm.get('reqLCD').value
    }else if(type == 'NB'){
      this.showCodeNB = this.myForm.get('reqNB').value
    }
  }

  onSubmit(){
    const formData = this.myForm.value
    formData.person1 = localStorage.getItem('rsesu')
    this._post.postITLoan(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text: res.event + " successfully registered",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.loading = false
          this.formInit()
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
          this.loading = false
        })
      }
    )
  }

  get dateBorrow(){ return this.myForm.get('dateBorrow') }
  get dateReturn(){ return this.myForm.get('dateReturn') }
  get codeLCD(){ return this.myForm.get('codeLCD') }
  get codeNB(){ return this.myForm.get('codeNB') }
  get person(){ return this.myForm.get('person') }
  get department(){ return this.myForm.get('department') }
  get event(){ return this.myForm.get('event') }
  get location(){ return this.myForm.get('location') }

}
