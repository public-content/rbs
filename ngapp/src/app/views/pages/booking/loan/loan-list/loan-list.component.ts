import { Component, OnInit } from '@angular/core';
import { GetService } from '../../../../../core/get.service'
import { PatchService } from '../../../../../core/patch.service'
import { DatePipe } from '@angular/common'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-loan-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.css']
})
export class LoanListComponent implements OnInit {

  loanList: any = []

  constructor(
    private datepipe: DatePipe,
    private _get : GetService,
    private _patch : PatchService
  ) { }

  ngOnInit(): void {
    this._get.getITLoan().subscribe(  data =>  this.loanList = data  )
  }

  convertDate(date,type){
    if(type == 'date'){
      let latest_date =this.datepipe.transform(date, 'yyyy-MM-dd');
      return latest_date
    }else if(type == 'time'){
      let latest_date =this.datepipe.transform(date, 'h:mm:ss a');
      return latest_date
    }
  }

  getLength(type){
   if(this.loanList && type == "loan"){
      return this.loanList.length
    }
  }


  onReturn(id, eventName){
    const formData: any = {}
    formData.id = id
    formData.event = eventName
    formData.person = localStorage.getItem('rsesu')
    Swal.fire({
      title: "Sure?",
      text: "Do you want to mark this as return",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes!"
    }).then((result) => {
        if (result.value) {
            this._patch.returnLoan(formData)
            .subscribe(
              res => {
                Swal.fire(
                  "Success!",
                  "Loan Assets has been returned.",
                  "success"
                )
                this.ngOnInit()
              },
              err =>{ console.log(err) }
            )
        }
    });
  }

}
