import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BaseComponent } from './base.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';



@NgModule({
  declarations: [BaseComponent, HeaderComponent, ContentComponent],
  exports:[  HeaderComponent, ContentComponent ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class BaseModule { }
