import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth-header',
  templateUrl: './auth-header.component.html',
  styleUrls: ['./auth-header.component.css']
})
export class AuthHeaderComponent implements OnInit {

  userName = JSON.parse(localStorage.getItem('detail')).fullname
  userNameFirst = this.userName.charAt(0)
  userDepartment = JSON.parse(localStorage.getItem('detail')).department

  constructor() { }

  ngOnInit(): void {
  }

}
