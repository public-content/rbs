import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthComponent } from './auth.component';
import { AuthContentComponent } from './auth-content/auth-content.component';
import { AuthHeaderComponent } from './auth-header/auth-header.component';
import { AuthNavComponent } from './auth-nav/auth-nav.component';
import { AuthProfileComponent } from './auth-profile/auth-profile.component';



@NgModule({
  declarations: [AuthComponent, AuthContentComponent, AuthHeaderComponent, AuthNavComponent, AuthProfileComponent],
  exports: [ AuthComponent, AuthContentComponent, AuthHeaderComponent, AuthNavComponent, AuthProfileComponent ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  bootstrap: [ AuthComponent, AuthContentComponent, AuthHeaderComponent, AuthNavComponent, AuthProfileComponent ]
})
export class AuthModule { }
