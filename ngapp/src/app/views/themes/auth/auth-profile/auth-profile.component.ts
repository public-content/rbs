import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../core/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PatchService } from '../../../../core/patch.service'
import Swal from 'sweetalert2'
import { UserManagementModule } from 'src/app/views/pages/user-management/user-management.module';

@Component({
  selector: 'app-auth-profile',
  templateUrl: './auth-profile.component.html',
  styleUrls: ['./auth-profile.component.css']
})
export class AuthProfileComponent implements OnInit {

  myForm: FormGroup
  alertMassage = ""
  userName = JSON.parse(localStorage.getItem('detail')).fullname
  userNameFirst = this.userName.charAt(0)
  userDepartment = JSON.parse(localStorage.getItem('detail')).department

  constructor(
    public _authService: AuthService,
    private fb: FormBuilder,
    private _patch: PatchService
  ) { }

  ngOnInit(): void {
    this.formInit()
  }

  formInit(){
    this.myForm = this.fb.group({
      password : ['', [Validators.required]],
      confirmPassword : ['', [Validators.required]]
    })
  }

  onSubmit(){
    const formData = this.myForm.value
    formData.id = JSON.parse(localStorage.getItem("detail"))._id
    formData.person = localStorage.getItem("rsesu")
    this._patch.editPassword(formData)
    .subscribe(
      res => {
        Swal.fire({
          title: "Yeahh!!",
          text:  "New password successfully updated",
          icon: "success",
          confirmButtonText: "Ok"
        }).then(()=> {
          this.formInit()
          console.log(res)
          let element: HTMLElement = document.getElementById('changePassModal') as HTMLElement;
          element.click();
        })
      },
      err => {
        Swal.fire({
          title: "Ohh no!!",
          text: err.message,
          icon: "warning",
          confirmButtonText: "Ok"
        }).then(()=> {
          console.log(err)
        })
      }
    )
  }

  checkPaswword(){
    let inputpassword = this.myForm.get("password").value
    let inputconfirmPassword = this.myForm.get("confirmPassword").value

    if(inputpassword != inputconfirmPassword){
      this.alertMassage = "Password does not match"
      this.myForm.get("confirmPassword").setErrors({'incorrect': true})
    }else{
      this.alertMassage = ""
    }
  }

  get password(){ return this.myForm.get('password') }
  get confirmPassword(){ return this.myForm.get('confirmPassword') }

}
