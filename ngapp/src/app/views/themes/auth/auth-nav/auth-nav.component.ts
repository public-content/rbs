import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth-nav',
  templateUrl: './auth-nav.component.html',
  styleUrls: ['./auth-nav.component.css']
})
export class AuthNavComponent implements OnInit {
  // userRole;
  constructor() { }

  ngOnInit(): void {
    // this.userRole == JSON.parse(localStorage.getItem('detail')).userLevel
  }

  privilege(role){
    let userRole = JSON.parse(localStorage.getItem('detail')).userLevel
    if(role.includes(userRole)){
      return true
    }else{
      return false
    }
  }

}
