import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { DatePipe } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './core/auth.service';
import { DeleteService } from './core/delete.service';
import { GetService } from './core/get.service';
import { PostService } from './core/post.service';
import { PatchService } from './core/patch.service';
import { BaseModule } from './views/themes/base/base.module';
import { AuthModule } from './views/themes/auth/auth.module';
import { AuthenticationModule } from './views/pages/authentication/authentication.module';
import { AuthGuard } from './views/pages/authentication/auth.guard';
import { BookingModule } from './views/pages/booking/booking.module';
import { CalendarModule } from './views/pages/calendar/calendar.module';
import { SetupModule } from './views/pages/setup/setup.module';
import { ReportModule } from './views/pages/report/report.module';
import { SystemSettingModule } from './views/pages/system-setting/system-setting.module';
import { UserManagementModule } from './views/pages/user-management/user-management.module';
import { TokenInterceptorService } from './core/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AuthModule,
    BaseModule,
    AppRoutingModule,

    // page module
    AuthenticationModule,
    BookingModule,
    CalendarModule,
    SetupModule,
    ReportModule,
    SystemSettingModule,
    UserManagementModule
  ],
  providers: [ DatePipe, AuthGuard, AuthService, DeleteService, GetService, PostService, PatchService, {
    provide : HTTP_INTERCEPTORS,
    useClass : TokenInterceptorService,
    multi : true
  } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
