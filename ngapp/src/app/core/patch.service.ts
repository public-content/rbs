import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PatchService {

 mainDock = "http://10.7.2.53:3000/"
 //mainDock = "http://localhost:3000/"

  constructor(private http: HttpClient) { }

  // delete system room =====================================================================================================
  private _delRoom = this.mainDock + "patch/del/room"
  delRoom(data){ return this.http.post<any>(this._delRoom, data) }
  // delete system room =====================================================================================================
  private _editRoom = this.mainDock + "patch/edit/room"
  editRoom(data){ return this.http.post<any>(this._editRoom, data) }


  // delete system Department ===============================================================================================
  private _delDepartment = this.mainDock + "patch/del/department"
  delDepartment(data){ return this.http.post<any>(this._delDepartment, data) }
  // delete system Department ===============================================================================================
  private _editDepartment = this.mainDock + "patch/edit/department"
  editDepartment(data){ return this.http.post<any>(this._editDepartment, data) }


  // delete system Table ====================================================================================================
  private _delTable = this.mainDock + "patch/del/table"
  delTable(data){ return this.http.post<any>(this._delTable, data) }
  // delete system Table ====================================================================================================
  private _editTable = this.mainDock + "patch/edit/table"
  editTable(data){ return this.http.post<any>(this._editTable, data) }


  // delete system Cafe =====================================================================================================
  private _delCafe = this.mainDock + "patch/del/cafe"
  delCafe(data){ return this.http.post<any>(this._delCafe, data) }
  // delete system Cafe =====================================================================================================
  private _editCafe = this.mainDock + "patch/edit/cafe"
  editCafe(data){ return this.http.post<any>(this._editCafe, data) }


  // delete system user =====================================================================================================
  private _delUser = this.mainDock + "patch/del/user"
  delUser(data){ return this.http.post<any>(this._delUser, data) }
  // delete system user =====================================================================================================
  private _editUser = this.mainDock + "patch/edit/user"
  editUser(data){ return this.http.post<any>(this._editUser, data) }


  // booking ITloan return ==================================================================================================
  private _returnLoan = this.mainDock + "patch/return/loan"
  returnLoan(data){ return this.http.post<any>(this._returnLoan, data) }
  // booking ITRoom return ==================================================================================================
  private _endRoom = this.mainDock + "patch/end/room"
  endRoom(data){ return this.http.post<any>(this._endRoom, data) }


  // delete holiday =========================================================================================================
  private _delHoliday = this.mainDock + "patch/del/holiday"
  delHoliday(data){ return this.http.post<any>(this._delHoliday, data)}
  // delete edit ============================================================================================================
  private _editHoliday = this.mainDock + "patch/edit/holiday"
  editHoliday(data){ return this.http.post<any>(this._editHoliday, data)}


  // Event edit =============================================================================================================
  private _editEvent = this.mainDock + "patch/edit/event"
  editEvent(data){ return this.http.post<any>(this._editEvent, data) }
  // Event edit =============================================================================================================
  private _delEvent = this.mainDock + "patch/delete/event"
  delEvent(data){ return this.http.post<any>(this._delEvent, data) }


  // oncall Edit ============================================================================================================
  private _editOncall = this.mainDock + "patch/edit/oncall"
  editOncall(data){ return this.http.post<any>(this._editOncall, data) }


  // profile password =======================================================================================================
  private _editPassword = this.mainDock + "patch/edit/password"
  editPassword(data){ return this.http.post<any>(this._editPassword, data) }


}
