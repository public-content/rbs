import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class GetService {

  mainDock = "http://10.7.2.53:3000/"
  // mainDock = "http://localhost:3000/"

  constructor(private http: HttpClient) { }

  // Getting IP Addresss ======================================================================================
  private _getIpAddress = this.mainDock+"get/ipaddress"
  getIpAddress(){ return this.http.get(this._getIpAddress) }

  // Active Event ============================================================================================= kene modify
  private _getAllEvents = this.mainDock+"get/active/masterEvents"
  getAllEvents(){ return this.http.get(this._getAllEvents) }

  // Active Event =============================================================================================
  private _getEventsInfo = this.mainDock+"get/a/masterEventsInfo/"
  getEventsInfo(date){ return this.http.get(this._getEventsInfo+date) }

  // Active IT loan =============================================================================================
  private _getITLoan = this.mainDock+"get/active/itloan"
  getITLoan(){ return this.http.get(this._getITLoan) }

  // Active IT Room =============================================================================================
  private _getITRoom = this.mainDock+"get/active/itroom"
  getITRoom(){ return this.http.get(this._getITRoom) }

  // Active - room event ======================================================================================
  private _getActiveVenue = this.mainDock+"get/active/room"
  getActiveVenue(){ return this.http.get(this._getActiveVenue) }

  // Active - table setup ======================================================================================
  private _getActiveSetup = this.mainDock+"get/active/setup"
  getActiveSetup(){ return this.http.get(this._getActiveSetup) }

  // Active - department ======================================================================================
  private _getActiveDepartment = this.mainDock+"get/active/department"
  getActiveDepartment(){ return this.http.get(this._getActiveDepartment) }

  // Active - cafe ======================================================================================
  private _getActiveCafe = this.mainDock+"get/active/cafe"
  getActiveCafe(){ return this.http.get(this._getActiveCafe) }

  // Active - Event(info) =====================================================================================
  private _getEventInfoFilter = this.mainDock+"get/filter/eventInfo/"
  getEventInfoFilter(date){ return this.http.get(this._getEventInfoFilter+date) }

  // all System Room ==========================================================================================
  private _getAllRoom = this.mainDock + "get/a/room"
  getAllRoom(){ return this.http.get(this._getAllRoom) }

  // all System Department ==========================================================================================
  private _getAllDepartment = this.mainDock + "get/a/department"
  getAllDepartment(){ return this.http.get(this._getAllDepartment) }

  // all System Table ==========================================================================================
  private _getAllTable = this.mainDock + "get/a/setup"
  getAllTable(){ return this.http.get(this._getAllTable) }

  // all System Cafe ==========================================================================================
  private _getAllCafe = this.mainDock + "get/a/cafe"
  getAllCafe(){ return this.http.get(this._getAllCafe) }

  // all System Holiday ==========================================================================================
  private _getAllHoliday = this.mainDock + "get/a/holiday"
  getAllHoliday(){ return this.http.get(this._getAllHoliday) }

  // all System User ==========================================================================================
  private _getAllUser = this.mainDock + "get/a/user"
  getAllUser(){ return this.http.get(this._getAllUser) }

  // single system room ======================================================================================
  private _getSingleRoom = this.mainDock+"get/s/room/"
  getSingleRoom(id){ return this.http.get(this._getSingleRoom+id) }

  // single system department ======================================================================================
  private _getSingleDepartment = this.mainDock+"get/s/department/"
  getSingleDepartment(id){ return this.http.get(this._getSingleDepartment+id) }

  // single system cafe ======================================================================================
  private _getSingleCafe = this.mainDock+"get/s/cafe/"
  getSingleCafe(id){ return this.http.get(this._getSingleCafe+id) }

  // single system table ======================================================================================
  private _getSingleTable = this.mainDock+"get/s/table/"
  getSingleTable(id){ return this.http.get(this._getSingleTable+id) }

  // user management  ======================================================================================
  private _getSingleUser = this.mainDock+"get/s/user/"
  getSingleUser(id){ return this.http.get(this._getSingleUser+id) }

  // single holiday ========================================================================================
  private _getSingleHoliday = this.mainDock+"get/s/holiday/"
  getSingleHoliday(id){ return this.http.get(this._getSingleHoliday+id) }

  // single Event ========================================================================================
  private _getSingleEvent = this.mainDock+"get/s/event/"
  getSingleEvent(id){ return this.http.get(this._getSingleEvent+id) }

  // oncall event ==========================================================================================
  private _getOncallList = this.mainDock+"get/a/oncall"
  getOncallList(){ return this.http.get(this._getOncallList) }

  // oncall single ==========================================================================================
  private _getOncallSingle = this.mainDock+"get/s/oncall/"
  getOncallSingle(id){ return this.http.get(this._getOncallSingle+id) }

  // report General =========================================================================================
  private _getAllGeneralReport = this.mainDock+"get/a/generalreport"
  getAllGeneralReport(){ return this.http.get(this._getAllGeneralReport) }

  // report Type ===========================================================================================
  private _getAllTypeReport = this.mainDock+"get/a/typeReport/"
  getAllTypeReport(type){ return this.http.get(this._getAllTypeReport+type) }

  // getting master history list by id ======================================================================
  private _getSingleHistory = this.mainDock+"get/s/masterHistory/"
  getSingleHistory(id){ return this.http.get(this._getSingleHistory+id) }




}
