import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  mainDock = "http://10.7.2.53:3000/"
  //mainDock = "http://localhost:3000/"

  constructor(private http: HttpClient, private _router : Router ) { }

  // login page ==================================================================================================
  private _postAuth = this.mainDock + "api/login"
  private _postOut = this.mainDock + "api/logout"

  loggedIn(){ return !!localStorage.getItem('token') }
  readyLoggedIn(){
    if(localStorage.getItem('token')){
      window.location.replace('sys')
    }
  }
  logoutUser() {
    let data = localStorage.getItem('rsesu')
    console.log(data)
    this.postOut({"name": data})
    .subscribe(
      res => {
        localStorage.removeItem('token')
        localStorage.removeItem('rsesu')
        localStorage.removeItem('detail')
        this._router.navigate([''])
        console.log(res)
      },
      err => {
        console.log(err)
      }
    )
  }
  getToken(){ return localStorage.getItem('token') }
  postAuth(data){ return this.http.post<any>(this._postAuth, data) }
  postOut(data){ return this.http.post<any>(this._postOut, data) }

}
