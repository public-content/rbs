import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PostService {

  mainDock = "http://10.7.2.53:3000/"
  //mainDock = "http://localhost:3000/"

  constructor(private http: HttpClient) { }

  // login page ==================================================================================================
  private _postAuth = this.mainDock + "api/login"
  postAuth(data){ return this.http.post<any>(this._postAuth, data) }

  // booking page ================================================================================================
  private _postBooking = this.mainDock + "post/booking"
  postBooking(data){ return this.http.post<any>(this._postBooking, data) }

  // system room register =========================================================================================
  private _postRoom = this.mainDock + "post/room"
  postRoom(data){ return this.http.post<any>(this._postRoom, data) }

  // system department register ===================================================================================
  private _postDepartment = this.mainDock + "post/department"
  postDepartment(data){ return this.http.post<any>(this._postDepartment, data) }

  // system table register ========================================================================================
  private _postTable = this.mainDock + "post/table"
  postTable(data){ return this.http.post<any>(this._postTable, data) }

  // system cafe register =========================================================================================
  private _postCafe = this.mainDock + "post/cafe"
  postCafe(data){ return this.http.post<any>(this._postCafe, data) }

  // system holiday register ======================================================================================
  private _postHoliday = this.mainDock + "post/holiday"
  postHoliday(data){ return this.http.post<any>(this._postHoliday, data) }

  // system user register =========================================================================================
  private _postUser = this.mainDock + "post/user"
  postUser(data){ return this.http.post<any>(this._postUser, data) }

  private _checkUsername = this.mainDock+"post/check/username"
  checkUsernameService(data){ return this.http.post<any>(this._checkUsername, data) }

  // booking it loan =========================================================================================
  private _postITLoan = this.mainDock+"post/ITLoan"
  postITLoan(data){ return this.http.post<any>(this._postITLoan, data) }

  // booking it room =========================================================================================
  private _postITRoom = this.mainDock+"post/ITRoom"
  postITRoom(data){ return this.http.post<any>(this._postITRoom, data) }

  // oncall register ================================ =========================================================
  private _postOncall = this.mainDock+"post/oncall"
  postOncall(data){ return this.http.post<any>(this._postOncall, data) }

}
