const express = require('express')
const router = express.Router()
const crypto = require('crypto');
const address = require('address')

// declaration -----------------------------------------------------------------------------
ip = address.ip()


// custom method or function ---------------------------------------------------------------
function getDate() {
    return new Date()
}

// model -----------------------------------------------------------------------------------
const Room = require('../models/room')
const Department = require('../models/department')
const Table = require('../models/table')
const Cafe = require('../models/cafe')
const MasterLogs = require('../models/masterLogs')
const User = require('../models/user')
const ITLoan = require('../models/itloan')
const ITRoom = require('../models/itroom')
const Holiday = require('../models/holiday')
const Booking = require('../models/booking')
const Info = require('../models/info')
const History = require('../models/history')
const Oncall = require('../models/oncall')

function getCrypto(data) {
    var hash = crypto.createHash('sha1')
    data = hash.update(data, 'utf-8')
    final = data.digest('hex')
    return final
}

function saveToLog(type, info, remarks, person) {
    logData = [];

    logData.type = type
    logData.data1 = info
    logData.data2 = remarks
    logData.person = person
    logData.ip = this.ip
    logData.created = getDate()
    logData.status = "active"

    return logData
}

function saveToLog2(type, info, remarks, person, ref) {
    logData = [];

    logData.type = type
    logData.data1 = info
    logData.data2 = remarks
    logData.data3 = ref
    logData.person = person
    logData.ip = this.ip
    logData.created = getDate()
    logData.status = "active"

    return logData
}

// router method -----------------------------------------------------------------------------------
router.get('/', (req, res) => {
    res.send('Post Patch')
})

// system room ===============================================================================
router.post('/del/room', async(req, res, next) => {
    let data = req.body
    try {
        const del = await Room.updateOne({ _id : data.id }, {
            $set : {
              status : data.status
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " a room: " + data.name
            let remarks = "Delete room"
            const log = await saveToLog('Delete', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})

router.post('/edit/room', async(req, res, next) => {
    let data = req.body
    try {
        const edit = await Room.updateOne({_id: data.id}, {
            $set : {
                roomName : data.roomName, 
                location : data.location, 
                capacity : data.capacity, 
                microphone : data.microphone, 
                audio : data.audio, 
                lcd : data.lcd,
            }
        })
        if(edit){
            let information = data.person + ' has Update room : ' + data.roomName
            let remarks = "Update Room"
            const log = await saveToLog('Update', information, remarks)
            let logModel = new MasterLogs(log)
            const logData = await logModel.save()
            if(logData){
                res.status(201).send(edit)
            }
        }        
    } catch (error) {
        next(error)
    }
})



// system department ===============================================================================
router.post('/del/department', async(req, res, next) => {
    let data = req.body
    try {
        const del = await Department.updateOne({ _id : data.id }, {
            $set : {
                status : data.status
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " a department: " + data.name
            let remarks = "Delete department"
            const log = await saveToLog('Delete', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})

router.post('/edit/department', async(req, res, next) => {
    let data = req.body
    try {
        const del = await Department.updateOne({ _id : data.id }, {
            $set : {
                departmentName : data.departmentName,
                departmentIncharge : data.departmentIncharge
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " has Update department : " + data.departmentName
            let remarks = "Update department"
            const log = await saveToLog('Delete', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})


// system table ===============================================================================
router.post('/del/table', async(req, res, next) => {
    let data = req.body
    try {
        const del = await Table.updateOne({ _id : data.id }, {
            $set : {
                status : data.status
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " a table setup: " + data.name
            let remarks = "Delete table setup"
            const log = await saveToLog('Delete', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})

router.post('/edit/table', async(req, res, next) => {
    let data = req.body
    try {
        const edit = await Table.updateOne({ _id : data.id }, {
            $set : {
                tableSetup : data.tableSetup
            }
        })
        if(edit){
            let information = data.person +' '+ data.msg + " has Update setup: " + data.tableSetup
            let remarks = "Update table setup"
            const log = await saveToLog('Delete', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(edit)
            }
        }
    } catch (error) {
        next(error)
    }
})




// system cafe ===============================================================================
router.post('/del/cafe', async(req, res, next) => {
    let data = req.body
    try {
        const del = await Cafe.updateOne({ _id : data.id }, {
            $set : {
                status : data.status
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " a cafe menu: " + data.name
            let remarks = "Delete cafe menu"
            const log = await saveToLog('Delete', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})

router.post('/edit/cafe', async(req, res, next) => {
    let data = req.body
    try {
        const del = await Cafe.updateOne({ _id : data.id }, {
            $set : {
                cafeMenu : data.cafeMenu
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " has Update cafe menu: " + data.cafeMenu
            let remarks = "Update cafe menu"
            const log = await saveToLog('Update', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){    
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})



// user management ===============================================================================
router.post('/del/user', async(req, res, next) => {
    let data = req.body
    try {
        const del = await User.updateOne({ _id : data.id }, {
            $set : {
                status : data.status
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " a user: " + data.name
            let remarks = "Delete user"
            const log = await saveToLog('Delete', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})

router.post('/edit/user', async(req, res, next) => {
    let data = req.body
    if(data.password != data.passwordOld){
        data.password = getCrypto(data.password)
    }
    try {
        const del = await User.updateOne({ _id : data.id }, {
            $set : {
                fullname : data.fullname,
                nickname : data.nickname,
                department : data.department,
                email : data.email,
                designation : data.designation,
                password : data.password,
                userLevel : data.userLevel,
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " has update a user: " + data.fullname
            let remarks = "Update user"
            const log = await saveToLog('Update', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})


// holiday  ===============================================================================
router.post('/del/holiday', async(req, res, next) => {
    let data = req.body
    try {
        const del = await Holiday.updateOne({ _id : data.id }, {
            $set : {
                status : data.status
            }
        })
        if(del){
            let information = data.person +' '+ data.msg + " a holiday: " + data.name
            let remarks = data.msg2 + "Holiday"
            const log = await saveToLog(data.msg2, information, remarks, data.person)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})

router.post('/edit/holiday', async(req, res, next) => {
    let data = req.body
    try {
        const del = await Holiday.updateOne({ _id : data.id }, {
            $set : {
                title : data.holidayTitle,
                date : data.holidayDate
            }
        })
        if(del){
            let information = data.person +' '+" has update a holiday: " + data.holidayTitle
            let remarks = "Update holiday"
            const log = await saveToLog('Update', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(del)
            }
        }
    } catch (error) {
        next(error)
    }
})


// booking page =========================================================================================
router.post('/return/loan', async(req, res, next) => {
    let data = req.body
    data.status = "returned"
    try {
        const rtn = await ITLoan.updateOne({ _id: data.id}, {
            $set : {
                status : data.status
            }
        })
        if(rtn){
            let information = data.person + " has confirm returning loan assest from : " + data.event
            let remarks = "Returned Loan"
            const log = await saveToLog('Return', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(rtn)
            }
        }
    } catch (error) {
        next(error)
    }
})

router.post('/end/room', async(req, res, next) => {
    let data = req.body
    data.status = "ended"
    try {
        const rtn = await ITLoan.updateOne({ _id: data.id}, {
            $set : {
                status : data.status
            }
        })
        if(rtn){
            let information = data.person + " has confirm ended : " + data.event
            let remarks = "Ended Room"
            const log = await saveToLog('Ended', information, remarks)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(rtn)
            }
        }
    } catch (error) {
        next(error)
    }
})


// Event Master ==========================================================================================
function setBooking(data) {
    const booking = {}
    let IT = ""
    let CAFE = ""
    let PR = ""

    if (data.reqIT == true) {
        IT = 'IT'
    } else {
        IT = ''
    }
    if (data.reqCafe == true) {
        CAFE = 'CF'
    } else {
        CAFE = ''
    }
    if (data.reqPark == true) {
        PR = 'PR'
    } else {
        PR = ''
    }

    booking.title = data.eventName + " @ " + data.venue
    booking.start = data.startDate
    booking.end = data.endDate
    booking.status = "active"
    booking.className = "event-state cust-display"
    booking.type = "booking"
    booking.dept = "HK " + IT + " " + CAFE + " " + PR
    booking.sort = data.startDate

    return booking
}
router.post('/edit/event', async(req, res, next) => {
    let data = req.body
    data.created = getDate()
    data.status = "active"
    let bookingData = await setBooking(data)
    try {
        const completeBooking = await Booking.updateOne({_id: data.eventID}, {
            $set : {
                title : bookingData.title,
                start : bookingData.start,
                end : bookingData.end,
                className : bookingData.className,
                type : bookingData.type,
                dept : bookingData.dept,
                sort : bookingData.sort,
                status : bookingData.status
            }
        })
        if(completeBooking.ok === 1){
            const completeInfo = await Info.updateOne({_id: data.infoID},{
                $set : {
                    startDate : data.startDate,
                    endDate : data.endDate,
                    duration : data.duration,
                    venue : data.venue,
                    layout : data.layout,
                    remarkHK : data.remarkHK,
                    eventName : data.eventName,
                    noPax : data.noPax,
                    requestBy : data.requestBy,
                    requestDepartment : data.requestDepartment,
                    reqIT : data.reqIT,
                    reqCafe : data.reqCafe,
                    reqPark : data.reqPark,
                    needNB : data.needNB,
                    needMic : data.needMic,
                    needPointer : data.needPointer,
                    needLcd : data.needLcd,
                    needAudio : data.needAudio,
                    remarkIT : data.remarkIT,
                    cafeMenu : data.cafeMenu,
                    remarkCafe : data.remarkCafe,
                    noValet : data.noValet,
                    noNormal : data.noNormal,
                    remarkValet : data.remarkValet,
                    remarkNormal : data.remarkNormal,
                    remarkOverall : data.remarkOverall,
                    masterEvents : data.eventID,
                    status : data.status
                }
            })
            if(completeInfo.nModified === 1 ){
                data.masterInfo = data.infoID
                let history = new History(data)
                const completeHistory = await history.save()
                if (completeHistory._id) {
                    let information = data.person + ' has update : ' + bookingData.title
                    let remarks = "Update Event"
                    const log = saveToLog2('Update', information, remarks, data.person, completeHistory._id)
                    let logModel = new MasterLogs(log)
                    const completeLog = await logModel.save()
                    if (completeLog._id) {
                        await Info.updateOne({_id: data.infoID},{
                            $set : {
                                created : data.created
                            }
                        })
                        let tell = {status: "Yes", msg : "Event has been Updated Successfully"}
                        res.status(201).send(tell)
                    }
                }
            }else{
                let tell = {status: "No", msg : "No changes has been made"}
                res.status(201).send(tell)
            }
        }
    } catch (error) {
        next(error)
    }                                       
})

router.post('/delete/event', async(req, res, next) => {
    let data = req.body
    try {
        const delInfo = await Info.updateOne({_id : data.infoID}, {
            $set : {
                status : data.status
            }
        })
        if(delInfo.nModified == 1){
            const delEvent = await Booking.updateOne({_id : data.eventID}, {
                $set : {
                    status : data.status
                }
            })
            if(delEvent.nModified == 1){
                let information = data.eventName + data.msg + data.person
                let remarks = data.statusLog+" Booking"
                const log = await saveToLog(data.statusLog, information, remarks, data.person)
                let logModel = new MasterLogs(log)
                const logSave = await logModel.save()
                if(logSave){
                    let cust = {}
                    cust.msg = "Event has been "+data.status
                    cust.sts = "Successfully"
                    res.status(201).send(cust)
                }
            }
        }

    } catch (error) {
        next(error)
    }
})



// Oncall Edit ==========================================================================================
function formatTitle(oncall, led){
    const val = "Oncall: "+oncall
    if(val.length > 25){
      const finalVal = val + " " + " LCD Duty: " + led
      return finalVal
    }else{
      let val_length = 25 - val.length
      let val_need_to_add = Array(val_length).join(".")
      const finalVal = val + " " + val_need_to_add + " " + " LCD Duty: " + led
      return finalVal
    }
}

router.post('/edit/oncall', async(req, res, next) => {
    let data = req.body
    data.title = formatTitle(data.oncall, data.led)

    try {
        const edit = await Oncall.updateOne({ _id : data.id }, {
            $set : {
                title : data.title,
                oncall : data.oncall,
                led : data.led
            }
        })
        if(edit){
            let information = data.date + " Oncall Setup has been Update by " + data.person
            let remarks = "Update Oncall Setup"
            const log = await saveToLog('Update', information, remarks, data.person)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(edit)
            }
        }
    } catch (error) {
        next(error)
    }

})

router.post('/edit/password', async(req, res, next) => {
    let data = req.body
    data.pass = getCrypto(data.password)

    try {
        const passChange = await User.updateOne({ _id: data.id }, {
            $set : {
                password :  data.pass
            }
        })
        if(passChange){
            let information = "Password change by " + data.person
            let remarks = "Change Password"
            const log = await saveToLog('Change Password', information, remarks, data.person)
            let logModel = new MasterLogs(log)
            const logSave = await logModel.save()
            if(logSave){
                res.status(201).send(passChange)
            }
        }
    } catch (error) {
        next(error)
    }
})

module.exports = router