const express = require('express')
const router = express.Router()
const crypto = require('crypto');
const jwt = require('jsonwebtoken')
const address = require('address')

// declaration -----------------------------------------------------------------------------
ip = address.ip()


// custom method or function ---------------------------------------------------------------
function getDate() {
    return new Date()
}

// model
const User = require('../models/user')
const MasterLogs = require('../models/masterLogs')

function saveToLog(type, info, remarks, person) {
    logData = [];

    logData.type = type
    logData.data1 = info
    logData.data2 = remarks
    logData.person = person
    logData.ip = this.ip
    logData.created = getDate()
    logData.status = "active"

    return logData
}

router.get('/', (req, res) => {
    res.send('Api Route')
})

//veerifyToken
function verifyToken(req, res, next){
    if(!req.headers.authorization) {
        return res.status(401).send('Unauthorized request')
      }
      let token = req.headers.authorization.split(' ')[1]
      if(token === 'null') {
        return res.status(401).send('Unauthorized request')    
      }
      let payload = jwt.verify(token, 'secretKey')
      if(!payload) {
        return res.status(401).send('Unauthorized request')    
      }
      req.userId = payload.subject
      next()
}


router.post('/login', async(req, res) =>{
    let userData = req.body
    var hash = crypto.createHash('sha1')
    data = hash.update(userData.password, 'utf-8')
    userData.password = data.digest('hex')
     User.findOne({username : userData.username}, (error, user) => {
        if(error){
            console.log(error)
        }else{
            if(!user){
                res.status(401).send('Invalid User')
            }else if(user.password !== userData.password){
                res.status(401).send('invalid password')
            }else if(user.status != 'active'){
                res.status(401).send('User Deactive')
            }else{
                let information = user.fullname + " has log in to the system"
                let remarks = "Login"
                const log =  saveToLog('Log', information, remarks, user.fullname)
                let logModel = new MasterLogs(log)
                const rlData =  logModel.save()
                let payload = { subject : user._id }
                let nickname = user.fullname
                let token = jwt.sign(payload, 'secretKey')
                res.status(200).send({token, nickname, user})
            }
        }
    })
})

router.post('/logout', async(req, res) => {
    let userName = req.body
    let information = userName.name + " has log out from the system"
    let remarks = "Logout"
    const log =  saveToLog('Log', information, remarks, userName.name)
    let logModel = new MasterLogs(log)
    const rlData =  logModel.save()
    res.status(200).send(rlData)
})


module.exports = router