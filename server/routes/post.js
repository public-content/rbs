const express = require('express')
const router = express.Router()
const crypto = require('crypto');
const address = require('address')

// declaration -----------------------------------------------------------------------------
ip = address.ip()


// custom method or function ---------------------------------------------------------------
function getDate() {
    return new Date()
}

function getCrypto(data) {
    var hash = crypto.createHash('sha1')
    data = hash.update(data, 'utf-8')
    final = data.digest('hex')
    return final
}

function saveToLog(type, info, remarks, person) {
    logData = [];

    logData.type = type
    logData.data1 = info
    logData.data2 = remarks
    logData.person = person
    logData.ip = this.ip
    logData.created = getDate()
    logData.status = "active"

    return logData
}

function saveToLog2(type, info, remarks, person, ref) {
    logData = [];

    logData.type = type
    logData.data1 = info
    logData.data2 = remarks
    logData.data3 = ref
    logData.person = person
    logData.ip = this.ip
    logData.created = getDate()
    logData.status = "active"

    return logData
}



// model -----------------------------------------------------------------------------------
const User = require('../models/user')
const MasterLogs = require('../models/masterLogs');
const Room = require('../models/room')
const Department = require('../models/department')
const Table = require('../models/table')
const Cafe = require('../models/cafe')
const Booking = require('../models/booking')
const Info = require('../models/info')
const History = require('../models/history')
const ITLoan = require('../models/itloan')
const ITRoom = require('../models/itroom')
const Holiday = require('../models/holiday')
const Oncall = require('../models/oncall')




// router method -----------------------------------------------------------------------------------
router.get('/', (req, res) => {
    res.send('Post Route')
})

router.post('/user', async (req, res, next) => {
    let formData = req.body
    formData.created = getDate()
    formData.status = "active"
    formData.password = getCrypto(formData.password)
    let user = new User(formData)

    try {
        const rData = await user.save()
        let information = formData.person + ' has register new user : ' + rData.fullname
        let remarks = "Register User"
        const log = await saveToLog('Register', information, remarks, formData.person)
        let logModel = new MasterLogs(log)
        const rlData = await logModel.save()
        res.status(201).send(rData)
    } catch (error) {
        next(error)
    }
})

router.post('/check/username', async(req, res, next) => {
    let userData = req.body
    await User.findOne({username:userData.username},
        (error, user)=>{
            if(error){
                console.log(error)
            }else{
                if(!user){
                    res.status(201).json({msg:"available"})
                }else{
                    res.status(201).json({msg:"notavailabele"})
                }
            }
        }
    )
})


// page Setting : Room -------------------------------------------------------------------------------
router.post('/room', async (req, res, next) => {
    let data = req.body
    data.created = getDate()
    data.status = "active"
    data.type = "room"
    let room = new Room(data)

    try {
        const rData = await room.save()
        let information = data.person + ' has register new room : ' + rData.roomName
        let remarks = "Register Room"
        const log = await saveToLog('Register', information, remarks, data.person)
        let logModel = new MasterLogs(log)
        const rlData = await logModel.save()
        res.status(201).send(rData)
    } catch (error) {
        next(error)
    }
})

// page Setting : Setup -------------------------------------------------------------------------------
router.post('/table', async (req, res, next) => {
    let data = req.body
    data.created = getDate()
    data.status = "active"
    data.type = "setup"
    let table = new Table(data)

    try {
        const rData = await table.save()
        let information = data.person + ' has register new table setup : ' + rData.tableSetup
        let remarks = "Register Table Setup"
        const log = await saveToLog('Register', information, remarks, data.person)
        let logModel = new MasterLogs(log)
        const rlData = await logModel.save()
        res.status(201).send(rData)
    } catch (error) {
        next(error)
    }
})

// page Setting : Cafe -------------------------------------------------------------------------------
router.post('/cafe', async (req, res, next) => {
    let data = req.body
    data.created = getDate()
    data.status = "active"
    data.type = "cafe"
    let cafe = new Cafe(data)

    try {
        const rData = await cafe.save()
        let information = data.person + ' has register new cafe menu : ' + rData.cafeMenu
        let remarks = "Register Cafe Menu"
        const log = await saveToLog('Register', information, remarks, data.person)
        let logModel = new MasterLogs(log)
        const rlData = await logModel.save()
        res.status(201).send(rData)
    } catch (error) {
        next(error)
    }
})

// page Setting : Department -------------------------------------------------------------------------------
router.post('/department', async (req, res, next) => {
    let data = req.body
    data.created = getDate()
    data.status = "active"
    data.type = "department"
    let department = new Department(data)

    try {
        const rData = await department.save()
        let information = data.person + ' has register new department : ' + rData.departmentName
        let remarks = "Register Department"
        const log = await saveToLog('Register', information, remarks, data.person)
        let logModel = new MasterLogs(log)
        const rlData = await logModel.save()
        res.status(201).send(rData)
    } catch (error) {
        next(error)
    }
})


// page Setting : Holiday ----------------------------------------------------------------------------------
router.post('/holiday', async(req, res, next) => {
    let data = req.body
    data.title = data.holidayTitle
    data.date = data.holidayDate
    data.className = "holiday-state cust-display flaticon-tea-cup"
    data.display = "background"
    data.type = "holiday"
    data.status = "active"
    data.dept = " "
    data.sort = data.holidayDate

    let holiday = new Holiday(data)

    try {
        const rHoliday = await holiday.save()
        if(rHoliday){
            let information = data.person + ' has register new holiday : ' + data.title
            let remarks = "Register Holiday"
            const log = await saveToLog('Register', information, remarks, data.person)
            let logModel = new MasterLogs(log)
            const rlData = await logModel.save()
            if(rlData){
                res.status(201).send(rHoliday)
            }
        }
    } catch (error) {
        next(error)
    }
})

// page booking : Event  -------------------------------------------------------------------------------

function setBooking(data) {
    const booking = {}
    let IT = ""
    let CAFE = ""
    let PR = ""

    if (data.reqIT == true) {
        IT = 'IT'
    } else {
        IT = ''
    }
    if (data.reqCafe == true) {
        CAFE = 'CF'
    } else {
        CAFE = ''
    }
    if (data.reqPark == true) {
        PR = 'PR'
    } else {
        PR = ''
    }

    booking.title = data.eventName + " @ " + data.venue
    booking.start = data.startDate
    booking.end = data.endDate
    booking.status = "active"
    booking.className = "event-state cust-display"
    booking.type = "booking"
    booking.dept = "HK " + IT + " " + CAFE + " " + PR
    booking.sort = data.startDate

    return booking
}


// Event Booking 
router.post('/booking', async(req, res, next) => {
    let data = req.body
    data.created = getDate()
    data.status = "active"
    let bookingData = setBooking(data)
    let booking = new Booking(bookingData)

    try {
        const completeBooking = await booking.save()
        if(completeBooking._id) {
            data.masterEvents = completeBooking._id
            let info = new Info(data)
            const completeInfo = await info.save()
            if (completeInfo._id) {
                // save a duplicate as history
                data.masterInfo = completeInfo._id
                let history = new History(data)
                const completeHistory = await history.save()
                if (completeHistory._id) {
                    let information = data.person + ' has booking new event : ' + completeBooking.title
                    let remarks = "Booking Event"
                    const log = saveToLog2('Booking', information, remarks, data.person, completeHistory._id)
                    let logModel = new MasterLogs(log)
                    const completeLog = await logModel.save()
                    if (completeLog._id) {
                        res.status(201).send(completeInfo)
                    }
                }
            }
        }
    } catch (error) {
        next(error)
    }
})



// ITLoan   -------------------------------------------------------------------------------
router.post('/ITLoan', async(req, res, next) => {
    let data = req.body
    data.created = getDate()
    data.status = "active"
    data.type = "loan"
    let loan = new ITLoan(data)
    try {
        const register = await loan.save()
        if(register){
            let information = data.person1 + ' has register new loan for event:' +register.event
            let remarks = "Register Loan"
            const log = await saveToLog('Register', information, remarks, data.person)
            let logModel = new MasterLogs(log)
            const logs = await logModel.save()
            if(logs){
                res.status(201).send(register)
            }
        }
    } catch (error) {
        next(error)
    }

    
})



// ITRoom   -------------------------------------------------------------------------------
router.post('/ITRoom', async(req, res, next) => {
    let data = req.body
    data.status = "active"
    data.type = "room"
    data.created = getDate()
    let itroom = new ITRoom(data)
    try {
        const register = await itroom.save()
        if(register){
            let information = data.person1 + ' has register new IT training room'
            let remarks = "Register IT training room"
            const log = await saveToLog('Register', information, remarks, data.person)
            let logModel = new MasterLogs(log)
            const logs = await logModel.save()
            if(logs){
                res.status(201).send(register)
            }
        }
    } catch (error) {
        next(error)
    }
})

// oncall --------------------------------------------------------------------------------

function formatTitle(oncall, led){
    const val = "Oncall: "+oncall
    if(val.length > 25){
      const finalVal = val + " " + " LCD Duty: " + led
      return finalVal
    }else{
      let val_length = 25 - val.length
      let val_need_to_add = Array(val_length).join(".")
      const finalVal = val + " " + val_need_to_add + " " + " LCD Duty: " + led
      return finalVal
    }
}

router.post('/oncall', async(req, res, next) => {
    let data = req.body
    data.title = formatTitle(data.oncall, data.led)
    data.date = data.date
    data.oncall = data.oncall
    data.led = data.led
    data.className = 'oncall-state cust-display flaticon-users-1'
    data.dept = "HK IT CF PR"
    data.sort = data.date + "T23:59"
    data.type = "oncall"
    data.status = "active"

    let oncall = new Oncall(data)
    try {
        const regOncall = await oncall.save()
        if(regOncall){
            let information = data.person + ' has setup oncall : ' + data.title
            let remarks = "Register Oncall"
            const log = await saveToLog('Register', information, remarks, data.person)
            let logModel = new MasterLogs(log)
            const rlData = await logModel.save()
            if(rlData){
                res.status(201).send(regOncall)
            }
        }
    } catch (error) {
        next(error)
    }
})



module.exports = router