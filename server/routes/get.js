const express = require('express')
const router = express.Router()
const address = require('address')


// model -----------------------------------------------------------------------------------
const User = require('../models/user')
const masterEvents = require('../models/masterEvent')
const MasterLogs = require('../models/masterLogs')
const ITloan = require('../models/itloan')
const ITRoom = require('../models/itroom')
const Room = require('../models/room')
const Department = require('../models/department')
const Table = require('../models/table')
const Cafe = require('../models/cafe')
const Info = require('../models/info')
const Holiday = require('../models/holiday')
const Oncall = require('../models/oncall')
const History = require('../models/history')



// router method -----------------------------------------------------------------------------------
router.get('/', (req, res) => {
    res.send('Get Route')
})

// geting ip address -------------------------------------------------------------------------------
router.get('/ipaddress', async(req, res, next) => {
    try {
        res.json(address.ip())
    } catch (error) {
        next(error)
    }
})

// page calendar ============================================================================
router.get('/active/masterEvents', async(req, res, next) => {
    try {
        await masterEvents.find({status : "active"}).exec((err, data) =>{
            if(err){ console.log(err) }else{ res.json(data) }
        })
    } catch (error) {
        next(error)
    }
})

router.get('/active/itloan', async(req, res, next) => {
    try {
        await ITloan.find({type : "loan" , status : "active"}).exec((error, data) =>{
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

router.get('/active/itroom', async(req, res, next) => {
    try {
        await ITRoom.find({type : "room" , status : "active"}).exec((error, data) =>{
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})


// get all oncall event ============================================================================
router.get('/a/oncall', async(req, res, next) => {
    try {
        await Oncall.find({type : "oncall"}).exec((err, data) =>{
            if(err){ console.log(err) }else{ res.json(data) }
        })
    } catch (error) {
        next(error)
    }
})

router.get('/s/oncall/:id', async(req, res, next) => {
    const id = req.params.id
    try {
        await Info.find({ masterEvents : id }).exec((err, data) => {
          if(err){
            res.json({notFound:1})
          }else{
            res.json(data)
          }
      })
      } catch (error) {
        next(error)
      }
})


// get active list : room ==========================================================================
router.get('/active/room', async(req, res, next) => {
    try {
        await Room.find({type : "room" , status : "active"}).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

// get all list : room ==========================================================================
router.get('/a/room', async(req, res, next) => {
    try {
        await Room.find({type : "room" }).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

// get single: room ==========================================================================
router.get('/s/room/:id', async(req, res, next) => {
    const id = req.params.id
    try {
        await Room.findById(id).exec((err, data) => {
          if(err){
            res.json({notFound:1})
          }else{
            res.json(data)
          }
      })
      } catch (error) {
        next(error)
      }
})

// get active list : setup ==========================================================================
router.get('/active/setup', async(req, res, next) => {
    try {
        await Table.find({type : "setup" , status : "active"}).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

// get all list : setup ==========================================================================
router.get('/a/setup', async(req, res, next) => {
    try {
        await Table.find({type : "setup" }).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

// get single: setup ==========================================================================
router.get('/s/table/:id', async(req, res, next) => {
    const id = req.params.id
    try {
        await Table.findById(id).exec((err, data) => {
          if(err){
            res.json({notFound:1})
          }else{
            res.json(data)
          }
      })
      } catch (error) {
        next(error)
      }
})

// get active list : department ==========================================================================
router.get('/active/department', async(req, res, next) => {
    try {
        await Department.find({type : "department" , status : "active"}).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

// get all list : department ==========================================================================
router.get('/a/department', async(req, res, next) => {
    try {
        await Department.find({type : "department" }).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

// get single: department ==========================================================================
router.get('/s/department/:id', async(req, res, next) => {
    const id = req.params.id
    try {
        await Department.findById(id).exec((err, data) => {
          if(err){
            res.json({notFound:1})
          }else{
            res.json(data)
          }
      })
      } catch (error) {
        next(error)
      }
})

// get active list : cafe ==========================================================================
router.get('/active/cafe', async(req, res, next) => {
    try {
        await Cafe.find({type : "cafe" , status : "active"}).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

// get all list : cafe ==========================================================================
router.get('/a/cafe', async(req, res, next) => {
    try {
        await Cafe.find({type : "cafe" }).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})

// get single: cafe ==========================================================================
router.get('/s/cafe/:id', async(req, res, next) => {
    const id = req.params.id
    try {
        await Cafe.findById(id).exec((err, data) => {
          if(err){
            res.json({notFound:1})
          }else{
            res.json(data)
          }
      })
      } catch (error) {
        next(error)
      }
})



// get all list : cafe ==========================================================================
router.get('/a/user', async(req, res, next) => {
    try {
        await User.find({}).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})


// get single: user ==========================================================================
router.get('/s/user/:id', async(req, res, next) => {
    const id = req.params.id
    try {
        await User.findById(id).exec((err, data) => {
          if(err){
            res.json({notFound:1})
          }else{
            res.json(data)
          }
        })
      } catch (error) {
        next(error)
      }
})


// get single: Holiday =========================================================================
router.get('/s/holiday/:id', async(req, res, next) => {
    const id = req.params.id
    try {
        await Holiday.findById(id).exec((err, data) => {
            if(err){
              res.json({notFound:1})
            }else{
              res.json(data)
            }
        })
    } catch (error) {
        next(error)
    }
})



// get event info single
router.get('/s/event/:id', async(req, res, next) => {
    let getID = req.params.id
    try {
        await Info.findById(getID).exec((err, data) => {
            if(err){
                res.json({notFound:1})
            }else{
                res.json(data)
            }
        })
    } catch (error) {
        next(error)
    }
} )
 


// get filter event ==== n ==== Booking : List Event =========================================================================
router.get('/filter/eventInfo/:date', async(req, res, next) => {
    let data = req.params.date
    const date = {}
    let current_datetime = new Date(data)
    // date.start = current_datetime.getFullYear() + "-" + ("0"+current_datetime.getMonth() + 1).slice(-2) + "-" + ("0"+ current_datetime.getDate()).slice(-2) + "T00:00"
    // date.end = current_datetime.getFullYear() + "-" + ("0"+current_datetime.getMonth() + 1).slice(-2) + "-" + ("0"+ current_datetime.getDate()).slice(-2) + "T23:59"
    date.start = data + "T00:00"
    date.end = data + "T23:59"
    //find date in between date
    try {
        await Info.find({"startDate":{$gte:date.start, $lt:date.end}}).sort({ startDate : 1 }).exec((err, dataa) => {
        if(err){
            res.json({ notFound: 1 })
        }else{
            res.json(dataa)
        } 
        })
    } catch (error) {
        next(error)
    }
})


// get all list : Holiday =====================================================================================
router.get('/a/holiday', async(req, res, next) => {
    const date = {}
    let d = new Date()
    date.start = d.getFullYear() + "-01-01"
    date.end = d.getFullYear() + "-12-31"
    try {
        await Holiday.find({ type : "holiday", "sort":{$gte:date.start, $lt:date.end} }).exec((error, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})


// get all list report : General ==============================================================================
router.get('/a/generalReport', async(req, res, next) => {
    try {
        await MasterLogs.find({}).limit(300).sort( { "_id" : -1  } ).exec((err, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})


// get all list report : By Type ==============================================================================
router.get('/a/typeReport/:type', async(req, res, next) => {
    let type = req.params.type
    try {
        await MasterLogs.find({"type": type }).limit(300).sort( { "_id" : -1  } ).exec((err, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})


// get single id history : by id ==============================================================================
router.get('/s/masterHistory/:id', async(req, res, next) => {
    let masterID = req.params.id
    try {
        await History.find({"masterInfo": masterID }).sort( { "_id" : -1  } ).exec((err, data) => {
            res.json(data)
        })
    } catch (error) {
        next(error)
    }
})


module.exports = router