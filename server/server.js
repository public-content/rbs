const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const cors = require('cors')

const api = require('./routes/api')
const r_post = require('./routes/post')
const r_get = require('./routes/get')
const r_patch = require('./routes/patch')

const db = ''
mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true }, err => {
    if(err){
        console.error('Error! ' + err)
    }else{
        console.log("Successfully connected to mongoDB!!!")
    }
})

const PORT = 3000
const app = express()
app.use(cors())
app.use(bodyParser.json())

app.use('/api', api)
app.use('/post', r_post)
app.use('/get', r_get)
app.use('/patch', r_patch)

app.get('/',(req, res) => {
    res.send('Hye from 500')
})

app.listen(PORT, () => {
    console.log('Server running on localhost: ' + PORT)
})