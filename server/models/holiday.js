const mongoose = require('mongoose')

const Schema = mongoose.Schema
const holidayScheme = new Schema({
    title : String,
    date : String,
    className : String,
    display : String,
    type : String,
    status : String,
    dept : String,
    sort: String
})

module.exports = mongoose.model('holiday', holidayScheme, 'masterEvents')