const mongoose = require('mongoose')

const Schema = mongoose.Schema
const logSchema = new Schema({
    type : String,
    data1 : String,
    data2 : String,
    data3 : String,
    data4 : String,
    data5 : String,
    person : String,
    ip : String,
    created : String,
    status : String
})

module.exports = mongoose.model('log', logSchema, 'masterLogs')