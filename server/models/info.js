const mongoose = require('mongoose')

const Schema = mongoose.Schema
const infosScheme = new Schema({
    startDate : String,
    endDate : String,
    duration : String,
    venue : String,
    layout : String,
    remarkHK : String,
    eventName : String,
    noPax : String,
    requestBy : String,
    requestDepartment : String,
    reqIT : Boolean,
    reqCafe : Boolean,
    reqPark : Boolean,
    needNB : Boolean,
    needMic : Boolean,
    needPointer : Boolean,
    needLcd : Boolean,
    needAudio : Boolean,
    remarkIT : String,
    cafeMenu : String,
    remarkCafe : String,
    noValet : String,
    noNormal : String,
    remarkValet : String,
    remarkNormal : String,
    remarkOverall : String,
    masterEvents : String,
    person_id : String,
    created : String,
    status : String
})

module.exports = mongoose.model('infos', infosScheme, 'masterInfo')