const mongoose = require('mongoose')

const Schema = mongoose.Schema
const tableSchema = new Schema({
    type : String,
    tableSetup : String,
    status : String,
    created: String
})

module.exports = mongoose.model('tables', tableSchema, 'masterSettings')