const mongoose = require('mongoose')

const Schema = mongoose.Schema
const cafeSchema = new Schema({
    type : String,
    cafeMenu : String,
    status : String,
    created : String
})

module.exports = mongoose.model('cafes', cafeSchema, 'masterSettings')