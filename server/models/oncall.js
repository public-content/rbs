const mongoose = require('mongoose')

const Schema = mongoose.Schema
const oncallScheme = new Schema({
    title : String,
    date : String,
    oncall : String,
    led : String,
    className : String,
    type : String,
    status : String,
    dept : String,
    sort: String
})

module.exports = mongoose.model('oncall', oncallScheme, 'masterEvents')