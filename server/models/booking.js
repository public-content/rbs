const mongoose = require('mongoose')

const Schema = mongoose.Schema
const bookingScheme = new Schema({
    title : String,
    start : String,
    end : String,
    className : String,
    type : String,
    dept : String,
    sort : String,
    status: String
})

module.exports = mongoose.model('bookings', bookingScheme, 'masterEvents')