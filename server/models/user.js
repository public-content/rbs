const mongoose = require('mongoose')

const Schema = mongoose.Schema
const userSchema = new Schema({
    fullname : String,
    nickname : String,
    department : String,
    designation : String,
    email : String,
    username : String,
    password : String,
    userLevel : String,
    status: String,
    created: String
})

module.exports = mongoose.model('user', userSchema, 'masterUsers')