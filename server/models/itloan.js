const mongoose = require('mongoose')

const Schema = mongoose.Schema
const loanSchema = new Schema({
    type : String,
    dateBorrow : String,
    dateReturn : String,
    reqLCD : String,
    reqNB : String,
    codeLCD : String,
    codeNB : String,
    person : String,
    department : String,
    event : String,
    location : String,
    remark : String,
    created : String,
    status : String
})

module.exports = mongoose.model('itloan', loanSchema, 'masterNotes')