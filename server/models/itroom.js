const mongoose = require('mongoose')

const Schema = mongoose.Schema
const itroomSchema = new Schema({
    type : String,
    dateStart : String,
    dateEnd : String,
    room : String,
    event : String,
    person : String,
    department : String,
    pax : String,
    remark : String,
    created : String,
    status : String
})

module.exports = mongoose.model('itroom', itroomSchema, 'masterNotes')