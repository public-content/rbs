const mongoose = require('mongoose')

const Schema = mongoose.Schema
const roomSchema = new Schema({
    type : String,
    roomName : String,
    location : String,
    capacity : String,
    microphone : Boolean,
    audio : Boolean,
    lcd : Boolean,
    status : String,
    created : String
})

module.exports = mongoose.model('rooms', roomSchema, 'masterSettings')