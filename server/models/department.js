const mongoose = require('mongoose')

const Schema = mongoose.Schema
const departmentSchema = new Schema({
    type : String,
    departmentName : String,
    departmentIncharge : String,
    status: String,
    created : String
})

module.exports = mongoose.model('department', departmentSchema, 'masterSettings')