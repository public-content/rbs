const mongoose = require('mongoose')

const Schema = mongoose.Schema
const EventsScheme = new Schema({
    title : String,
    date: String,
    start : String,
    end : String,
    className : String,
    display : String,
    type : String,
    status: String,
    sort: String,
})

module.exports = mongoose.model('masterEvent', EventsScheme, 'masterEvents')